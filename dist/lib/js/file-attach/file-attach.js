$(window).resize(function() {
    setImageHeight();
});

function buildFileItem(objImageData) {
    $('#file_attach').remove();

    var elementBuild = '';

    elementBuild += '<div id="file_attach" class="file-attach">';
    elementBuild += '<div class="navbar navbar-inverse">';
    elementBuild += '<div class="navbar-header">';
    elementBuild += '<span class="navbar-brand"><i id="file_type" class="position-left"></i> <span id="file_name"></span></span>';
    elementBuild += '</div>';
    elementBuild += '<div class="navbar-collapse collapse">';
    elementBuild += '<ul class="nav navbar-nav navbar-right">';
    elementBuild += '<li id="zoom_in"><a href="javascript:;" onclick="zoomInImage()"><i class="icon-zoomin3 position-left"></i> Zoom In</a></li>';
    elementBuild += '<li id="zoom_out"><a href="javascript:;" onclick="zoomOutImage()"><i class="icon-zoomout3 position-left"></i> Zoom Out</a></li>';
    elementBuild += '<li id="previous_file"><a href="javascript:;" onclick="previousFileImage()"><i class="icon-arrow-left7 position-left"></i> Previous</a></li>';
    elementBuild += '<li id="next_file"><a href="javascript:;" onclick="nextFileImage()">Next <i class="icon-arrow-right7 position-right"></i></a></li>';
    elementBuild += '<li><a href="javascript:;"><i class="icon-download4 position-left"></i> Download</a></li>';
    elementBuild += '<li><a href="javascript:;"><i class="icon-trash position-left"></i> Delete</a></li>';
    elementBuild += '<li><a href="javascript:;" onclick="closeFileImage()"><i class="icon-cross2 position-left"></i> Close</a></li>';
    elementBuild += '</ul>';
    elementBuild += '</div>';
    elementBuild += '</div>';
    elementBuild += '<div class="file-view">';

    $.each(objImageData, function(key, value) {
        elementBuild += '<div id="file_' + key + '" class="file-item">';
        elementBuild += '<img src="' + value.src + '" alt="' + value.alt + '" class="file-item-image center-block">';
        elementBuild += '</div>';
    });

    elementBuild += '</div>';
    elementBuild += '</div>';

    $('body').append(elementBuild);
}

function setImageHeight() {
    var setImageHeight = ($('.file-attach').height() - 70);
    $('.file-item-image').css('height', setImageHeight + 'px');
}

function openFileImage(element) {
    $.fileAttachScale = 1;

    $('.file-item').first().addClass('file-item-first');
    $('.file-item').last().addClass('file-item-last');

    var countElement = $('.file-item').length;
    $('#next_file').removeClass('disabled');
    $('#previous_file').removeClass('disabled');
    if (countElement <= 1) {
        $('#next_file').addClass('disabled');
        $('#previous_file').addClass('disabled');
    }

    var oldElement = $('#' + element);

    if (oldElement.hasClass('file-item-first') && !$('#previous_file').hasClass('disabled')) {
        $('#previous_file').addClass('disabled');
    }

    if (oldElement.hasClass('file-item-last') && !$('#next_file').hasClass('disabled')) {
        $('#next_file').addClass('disabled');
    }

    $('.file-item').removeClass('active');
    oldElement.addClass('active');

    var fileName = oldElement.find('img').attr('alt');
    $('#file_name').html(fileName);

    oldElement.find('img').draggable();
}

function nextFileImage() {
    $.fileAttachScale = 1;
    var firstElement = $('.file-item-first');
    var lastElement = $('.file-item-last');
    var oldElement = $('.file-item.active');
    var oldElementImage = oldElement.find('img');
    var nextElement = oldElement.next('.file-item');

    oldElementImage.css('-webkit-transform', 'scale(' + $.fileAttachScale + ')');
    oldElementImage.css('-moz-transform', 'scale(' + $.fileAttachScale + ')');
    oldElementImage.css('-ms-transform', 'scale(' + $.fileAttachScale + ')');
    oldElementImage.css('transform', 'scale(' + $.fileAttachScale + ')');
    oldElementImage.css('-webkit-transform-origin', 'top center');
    oldElementImage.css('-moz-transform-origin', 'top center');
    oldElementImage.css('-ms-transform-origin', 'top center');
    oldElementImage.css('transform-origin', 'top center');
    oldElementImage.css('left', '');
    oldElementImage.css('top', '');

    if (!nextElement.hasClass('file-item-first') && $('#previous_file').hasClass('disabled')) {
        $('#previous_file').removeClass('disabled');
    }

    if ($('#next_file').hasClass('disabled')) {
        return;
    }

    if (nextElement.hasClass('file-item-last')) {
        $('#next_file').addClass('disabled');
    }

    nextElement.addClass('active');
    oldElement.removeClass('active');

    $(".file-item.active img").draggable();

    var fileName = nextElement.find('img').attr('alt');
    $('#file_name').html(fileName);
    
}

function previousFileImage() {
    $.fileAttachScale = 1;
    var firstElement = $('.file-item-first');
    var lastElement = $('.file-item-last');
    var oldElement = $('.file-item.active');
    var oldElementImage = oldElement.find('img');
    var prevElement = oldElement.prev('.file-item');

    oldElementImage.css('-webkit-transform', 'scale(' + $.fileAttachScale + ')');
    oldElementImage.css('-moz-transform', 'scale(' + $.fileAttachScale + ')');
    oldElementImage.css('-ms-transform', 'scale(' + $.fileAttachScale + ')');
    oldElementImage.css('transform', 'scale(' + $.fileAttachScale + ')');
    oldElementImage.css('-webkit-transform-origin', 'top center');
    oldElementImage.css('-moz-transform-origin', 'top center');
    oldElementImage.css('-ms-transform-origin', 'top center');
    oldElementImage.css('transform-origin', 'top center');
    oldElementImage.css('left', '');
    oldElementImage.css('top', '');

    if (!prevElement.hasClass('file-item-last') && $('#next_file').hasClass('disabled')) {
        $('#next_file').removeClass('disabled');
    }

    if ($('#previous_file').hasClass('disabled')) {
        return;
    }

    if (prevElement.hasClass('file-item-first')) {
        $('#previous_file').addClass('disabled');
    }

    prevElement.addClass('active');
    oldElement.removeClass('active');

    $(".file-item.active img").draggable();

    var fileName = prevElement.find('img').attr('alt');
    $('#file_name').html(fileName);
}

function zoomInImage() {
    var Element = $('.file-item.active').find('img');

    if ($.fileAttachScale < 2.2) {
        $.fileAttachScale += 0.4;

        Element.css('-webkit-transform', 'scale(' + $.fileAttachScale + ')');
        Element.css('-moz-transform', 'scale(' + $.fileAttachScale + ')');
        Element.css('-ms-transform', 'scale(' + $.fileAttachScale + ')');
        Element.css('transform', 'scale(' + $.fileAttachScale + ')');
        Element.css('-webkit-transform-origin', 'top center');
        Element.css('-moz-transform-origin', 'top center');
        Element.css('-ms-transform-origin', 'top center');
        Element.css('transform-origin', 'top center');
    }
}

function zoomOutImage() {
    var Element = $('.file-item.active').find('img');

    if ($.fileAttachScale > 1) {
        $.fileAttachScale -= 0.4;

        Element.css('-webkit-transform', 'scale(' + $.fileAttachScale + ')');
        Element.css('-moz-transform', 'scale(' + $.fileAttachScale + ')');
        Element.css('-ms-transform', 'scale(' + $.fileAttachScale + ')');
        Element.css('transform', 'scale(' + $.fileAttachScale + ')');
        Element.css('-webkit-transform-origin', 'top center');
        Element.css('-moz-transform-origin', 'top center');
        Element.css('-ms-transform-origin', 'top center');
        Element.css('transform-origin', 'top center');
    }

    if ($.fileAttachScale <= 1) {
        Element.css('left', '0px');
        Element.css('top', '0px');
    }
}

function closeFileImage() {
    $('#file_attach').remove();
}