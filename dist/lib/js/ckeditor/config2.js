﻿/**
* @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
* For licensing, see LICENSE.md or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function (config) {

    config.toolbarGroups = [
		{ name: 'others', groups: ['others'] },
		{ name: 'forms', groups: ['forms'] },
		{ name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
		{ name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph'] },
		{ name: 'links', groups: ['links'] },
		{ name: 'colors', groups: ['colors'] },
		{ name: 'about', groups: ['about'] }
	];

    config.removeButtons = 'Cut,Copy,Paste,Anchor,Subscript,RemoveFormat,Blockquote';

    config.uiColor = '#fafafa';
    config.toolbarLocation = 'bottom';
    config.enterMode = CKEDITOR.ENTER_BR;
};
