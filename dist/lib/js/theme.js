﻿$('.jqueryDatetimePicker').parent().find('.glyphicon.glyphicon-calendar.form-control-feedback').click(function () {
    $(this).siblings('.jqueryDatetimePicker').focus();
});


$('.jqueryDatetimePicker').keyup(function (e) {
    if (e.keyCode == 8)
        $(this).val('');
});

$(window).load(function () {
    $('.jqueryDatetimePicker:not([disabled]):not([readonly]):not(.hidden)').parent().append('<i class="fa fa-close cleardatetimepicker pointer" style="position:absolute; top:7px; right:45px; z-index:1;"></i>');
    $('.cleardatetimepicker').click(function () {
        $(this).siblings('.jqueryDatetimePicker').val('');
    }); 

});

/* highlight v4 */
jQuery.fn.highlight = function (c) { function e(b, c) { var d = 0; if (3 == b.nodeType) { var a = b.data.toUpperCase().indexOf(c); if (0 <= a) { d = document.createElement("span"); a = b.splitText(a); a.splitText(c.length); var f = a.cloneNode(!0); d.appendChild(f); a.parentNode.replaceChild(d, a); d = 1 } } else if (1 == b.nodeType && b.childNodes && !/(script|style)/i.test(b.tagName)) for (a = 0; a < b.childNodes.length; ++a) a += e(b.childNodes[a], c); return d } return this.length && c && c.length ? this.each(function () { e(this, c.toUpperCase()) }) : this }; jQuery.fn.removeHighlight = function () { return this.find("span").each(function () { this.parentNode.firstChild.nodeName; with (this.parentNode) replaceChild(this.firstChild, this), normalize() }).end() };
function listHighlight() {
    $('#grid tbody tr').each(function () {
        var titlename = $(this).children('td').eq(1).text();
        var objectid = $(this).children('td').eq(2).text();
        $(this).attr('titlename', titlename);
        $(this).attr('objectid', objectid);
    });

    $("#grid td:not(:last-child)").highlight($('#txtSearch').val());    
}





//$('.btn.btn-sub:contains("Refresh")').css('color', '#0000CD');
//$('.btn.btn-sub:contains("Delete")').css('color', '#8B0000');
//$('.btn.btn-sub .glyphicon-plus-sign').parent().css('color', '#006400');
















var theme = (function () {
    var myfunction = {};
    var nodata = 'No data available.';

    // Datepicker
    var loopAB = function (_node, _data) {
        var data = _data;
        var node = $('#' + _node);
        if (_data.length > 0) {
            node.addClass('loop-ab');
            for (i = 0; i < data.length; i++) {
                node.append('<div class="item item-' + i + '">' + loopAB_item + '</div>');
                node.find('.item-' + i + ' .thumb').html(data[i].thumb);
                node.find('.item-' + i + ' .detail').html(data[i].detail);
            }
        } else {
            node.append(nodata);
        }
    }
    var loopAB_item = '<div class="thumb"></div><div class="detail"></div>';
    myfunction.loopAB = loopAB;

    return myfunction;
} ());









$('[onclick*="OnclickPopup"]').each(function () {
    $(this).siblings('input').attr('readonly', true);
});












