$(document).ready(function () {
    var calenderWidth = $('.tungjai_calender').width();
    $('tr.tungjai_calender-content_event td').css('width', (calenderWidth / 7) + 'px');

    var date = new Date();
    monthNavPrevious = date.getMonth();
    monthNavNext = (date.getMonth() + 2);
    currentMonth = (date.getMonth() + 1);
    currentYear = date.getFullYear();
    getTheDays((date.getMonth() + 1), currentYear);

    $('#currentMonth').val(currentMonth);
});

$(window).resize(function() {
    var calenderWidth = $('.tungjai_calender').width();
    $('tr.tungjai_calender-content_event td').css('width', (calenderWidth / 7) + 'px');
});

function DemogetTheDays(month, year) {    
    var demo_day = '';
    var dt = new Date(month + '/01/' + year);
    var dt2 = new Date();
    var dayNow = dt2.getDate();
    var monthNow = dt2.getMonth();
    var yearNow = dt2.getFullYear();
    var month = dt.getMonth();
    var year = dt.getFullYear();
    var FirstDay = new Date(year, month, 1);
    var LastDay = new Date(year, month + 1, 0);
    var weekday = new Array();
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";

    if (typeof weekday[FirstDay.getDay()] != 'undefined') { 
        demo_FirstDay = FirstDay.toDateString('d/m/yyyy');
        demo_LastDay = LastDay.toDateString('d/m/yyyy');
        demo_split_FirstDay = demo_FirstDay.split(' ');
        demo_split_LastDay = demo_LastDay.split(' ');

        $('.tungjai_calender-header_center h3').html(replaceCalendarToThai(demo_split_FirstDay[1], year));
//        $('.tungjai_calender-header_center h3').html(demo_split_FirstDay[1] + ' ' + year);

        var demo_current_day = (FirstDay.getDay() + 1);
        var demo_inner_event = '';

        for (var i = 1; i <= demo_split_LastDay[2]; i++) {
            demo_inner_event = '';
            demo_inner_event += '<span>' + i + '</span>';
            

//            switch (i % 5) {
//                case 1:
//                    demo_inner_event += '<div><img src="../assets/images/calendar/Holiday.png" alt="" title="Holiday"></div>';
//                    break; 
//                case 2:
//                    demo_inner_event += '<div><img src="../assets/images/calendar/Leave-Approve.png" alt="" title="Leave Approve"></div>';
//                    break; 
//                case 3:
//                    demo_inner_event += '<div><img src="../assets/images/calendar/Birthday.png" alt="" title="Birthday"></div>';
//                    break; 
//                default: 
//                    demo_inner_event += '';
//            }

            if (month == monthNow && year == yearNow && (i - FirstDay.getDay()) == dayNow) {
                $('#dt' + i).addClass('current_day');
            } else {
                $('#dt' + i).removeClass('current_day');
            }

            if ((i % 5) == 3) { //
                $('#dt' + demo_current_day).addClass('activity_day');
            } else {
                $('#dt' + demo_current_day).removeClass('activity_day');
            }

            document.getElementById('dt' + demo_current_day).innerHTML = demo_inner_event;
            demo_current_day++;

            if ($('#dt36').is(':empty')) {
                $('.tungjai_calender-content_event:last-child').css('display', 'none');
            } else {
                $('.tungjai_calender-content_event:last-child').css('display', '');
            }
        }
    }
}

function monthPrevious() {    
    $('tr.tungjai_calender-content_event td').html('');
    var date = new Date();
    getTheDays(monthNavPrevious, currentYear);
    monthNavPrevious--;
    monthNavNext = monthNavPrevious + 2;

    if (monthNavPrevious == 0) {
        currentYear--;
        monthNavPrevious = 12;
    }

    if (monthNavNext == 13) {
        if (monthNavPrevious == 12) {
            currentYear++;
        }
        monthNavNext = 1;
    }
}

function monthNext() {
    $('tr.tungjai_calender-content_event td').html('');
    var date = new Date();
    getTheDays(monthNavNext, currentYear);
    monthNavPrevious = (monthNavNext - 1);
    monthNavNext++;

    if (monthNavNext == 13) {
        currentYear++;
        monthNavNext = 1;
    }

    if (monthNavPrevious == 0) {
        if (monthNavNext == 1) {
            currentYear--;
        }
        monthNavPrevious = 12;
    }
}

function currentDateToday() {
    var date = new Date();
    monthNavPrevious = date.getMonth();
    monthNavNext = (date.getMonth() + 2);
    currentYear = date.getFullYear();
    getTheDays((date.getMonth() + 1), currentYear);
}

function planClick(element) {
    $('.btn-view').removeClass('active');
    $(element).addClass('active');
}
