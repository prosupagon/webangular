var royal_tab_api = null;

jQuery(document).ready(function($) {

    royal_tab_api = new Royal_Tab_Api($('div#main-royal-tab.royal_tab'));

});

window.addEventListener('resize', resizeAction);
function resizeAction() {
    $("#main-content").css("min-height", $(window).height() - 40);
    $("#left-navigation").css("height", $(window).height() - 40);

    var leftNavHeight = $("#left-navigation").height();
    var innerNavHeight = $(".mainNav").height();

    // if (leftNavHeight < innerNavHeight) {
    //     $("#left-navigation").css("overflow-y", "scroll");
    //     $(".mainNav").css("width", "214px");
    // } else {
    //     $("#left-navigation").css("overflow-y", "visible");
        
    //     if ($("#left-navigation").hasClass("active")) {
    //         $(".mainNav").css("width", "70px");
    //     } else {
    //         $(".mainNav").css("width", "230px");
    //     }
    // }

    var frame = document.getElementsByClassName("iframe_main");
    var i;
    for (i = 0; i < frame.length; i++) {
        frame[i].height = $(window).height() - 120;
        frame[i].style.minHeight = ($(window).height() - 120) + "px";
    }
};

function ResizeIframe(id) {
    var frame = document.getElementById(id);
    var style;
    var minHeight;
    frame.height = frame.contentWindow.document.body.scrollHeight;
    if (window.getComputedStyle) {
        style = window.getComputedStyle(frame);
        minHeight = style.getPropertyValue('min-height');
    }
    minHeight = minHeight.replace("px", "");
    var myHeight = frame.contentWindow.document.body.scrollHeight;
    if (myHeight < minHeight) {
        frame.height = minHeight;
    }

    $("#main-content").css("min-height", (parseInt(frame.height) + 120) + "px");

    resizeAction();
    
}

function openTab(tabId, title, url, icon, breadcrumb) {
    var hTabId = "h_" + tabId;  // Tab Header ID
    var cTabId = "c_" + tabId;  // Tab Content ID
    var fTabId = "iframe_" + tabId;  // iFrame ID

    if (!document.getElementById(hTabId)) {
        var newTabContent = '';

        // Breadcrumb Builder
        if (typeof breadcrumb !== "undefined" && breadcrumb !== null) {
            newTabContent += '<div class="col-xs-12">'
            newTabContent += '<div class="page-breadcrumb">';
            for (var i = 0; i < breadcrumb.length; i++) {
                if (i === (breadcrumb.length - 1)) {
                    newTabContent += '<span class="current">' + breadcrumb[i].title + '</span>';
                } else {
                    newTabContent += '<a href="javascript:;">' + breadcrumb[i].title + '</a>';
                }
            }
            newTabContent += '</div>';
            newTabContent += '</div>';
        }

        newTabContent += "<iframe id=\"" + fTabId + "\" class=\"iframe_main\" src=\"" + url + "\"  frameborder=\"0\" scrolling=\"yes\" onload=\"ResizeIframe('" + fTabId + "')\" style=\"width: 100%;\">";
            newTabContent += "<p>Your browser does not support iframes.</p>";
            newTabContent += "</iframe>";

        royal_tab_api.addCustom(0, true, title + "<i class=\"fa fa-times royal-tab-close fa-fw\" onclick=\"royal_tab_api.removeCustom('" + tabId + "');\"></i>", newTabContent, tabId);
    } else {
        var emptyTab = royal_tab_api.removeCustom(tabId);
        if (emptyTab == 0) {
            return;
        }

        var newTabContent = '';

        // Breadcrumb Builder
        if (typeof breadcrumb !== "undefined" && breadcrumb !== null) {
            newTabContent += '<div class="col-xs-12">'
            newTabContent += '<div class="page-breadcrumb">';
            for (var i = 0; i < breadcrumb.length; i++) {
                if (i === (breadcrumb.length - 1)) {
                    newTabContent += '<span class="current">' + breadcrumb[i].title + '</span>';
                } else {
                    newTabContent += '<a href="javascript:;" onclick="openTab(' + 1, 'Dashboard', '../Dashboard/dashboard.html', 'fa-tachometer', [{'title':'Site Link 1', 'url':'../Dashboard/dashboard.html', 'id':'18888'},{'title':'Site Link 2', 'url':'../Organization/OrganizationChart.html', 'id':'19888'},{'title':'Site Link 3', 'url':'../Employee/EmpMenuList.html', 'id':'20888'},{'title':'Current Site', 'url':'../TimeAttendance/TimeAttendanceMenuList.html', 'id':'21888'}] + ')">' + breadcrumb[i].title + '</a>';
                }
            }
            newTabContent += '</div>';
            newTabContent += '</div>';
        }

        newTabContent = "<iframe id=\"" + fTabId + "\" class=\"iframe_main\" src=\"" + url + "\"  frameborder=\"0\" scrolling=\"yes\" onload=\"ResizeIframe('" + fTabId + "')\" style=\"width: 100%;\">";
            newTabContent += "<p>Your browser does not support iframes.</p>";
            newTabContent += "</iframe>";

        royal_tab_api.addCustom(0, true, title + "<i class=\"fa fa-times royal-tab-close fa-fw\" onclick=\"royal_tab_api.removeCustom('" + tabId + "');\"></i>", newTabContent, tabId);
    }
}

// function defaultTab() {
//     var tabId = 1;
//     var hTabId = "h_" + tabId;
//     var cTabId = "c_" + tabId;
//     var fTabId = "f_" + tabId;
//     var title = "Dashboard";
//     var url = "../Dashboard/dashboard.html";
//     var icon = "fa-dashboard";

//     var newTabList = "<li id=\"" + hTabId + "\"><a class=\"tab-header\" href=\"#" + cTabId + "\" data-toggle=\"tab\"><i class=\"fa " + icon + " mgr5\"></i><span>" + title + "</span><i class=\"fa fa-times tab-list-close\" onclick=\"removeTab('" + tabId + "')\"></i></a></li>";

//     $("#main-tab-list li").removeClass("active");
//     $("#main-tab-list").append(newTabList);

//     var newTabContent = "<div id=\"" + cTabId + "\" class=\"tab-pane\">";
//     newTabContent += "<div class=\"iframe-block\">";
//     newTabContent += "<div class=\"iframe-body\">";
//     newTabContent += "<iframe id=\"" + fTabId + "\" class=\"iframe_main\" src=\"" + url + "\" frameborder=\"0\" onload=\"ResizeIframe('" + fTabId + "')\">";
//     newTabContent += "<p>Your browser does not support iframes.</p>";
//     newTabContent += "</iframe>";
//     newTabContent += "</div>";
//     newTabContent += "</div>";
//     newTabContent += "</div>";

//     $("#main-tab-content").append(newTabContent);
//     $("#" + fTabId).css("min-height", (window.screen.availHeight - 105) + "px");

//     var newElmActive = $("#main-tab-list li#" + hTabId + " a");
//     newElmActive.click();
// }

// function openTab(tabId, title, url, icon) {
//     var hTabId = "h_" + tabId;  // Tab Header ID
//     var cTabId = "c_" + tabId;  // Tab Content ID
//     var fTabId = "f_" + tabId;  // iFrame ID

//     if (document.getElementById(hTabId)) {
//         $("#" + hTabId).remove();
//         $("#" + cTabId).remove();

//         var newTabList = "<li id=\"" + hTabId + "\"><a class=\"tab-header\" href=\"#" + cTabId + "\" data-toggle=\"tab\"><i class=\"fa " + icon + " mgr5\"></i><span>" + title + "</span><i class=\"fa fa-times tab-list-close\" onclick=\"removeTab('" + tabId + "')\"></i></a></li>";

//         var newTabContent = "<div id=\"" + cTabId + "\" class=\"tab-pane\">";
//         newTabContent += "<div class=\"iframe-block\">";
//         newTabContent += "<div class=\"iframe-body\">";
//         newTabContent += "<iframe id=\"" + fTabId + "\" class=\"iframe_main\" src=\"" + url + "\"  frameborder=\"0\" onload=\"ResizeIframe('" + fTabId + "')\">";
//         newTabContent += "<p>Your browser does not support iframes.</p>";
//         newTabContent += "</iframe>";
//         newTabContent += "</div>";
//         newTabContent += "</div>";
//         newTabContent += "</div>";

//         $("#main-tab-list li").removeClass("active");
//         $("#main-tab-list").append(newTabList);

//         $("#main-tab-content").append(newTabContent);
//         $("#" + fTabId).css("min-height", (window.screen.availHeight - 105) + "px");

//         var newElmActive = $("#main-tab-list li#" + hTabId + " a");
//         newElmActive.click();
//     } else {
//         var tabList = "<li id=\"" + hTabId + "\"><a class=\"tab-header\" href=\"#" + cTabId + "\" data-toggle=\"tab\"><i class=\"fa " + icon + " mgr5\"></i><span>" + title + "</span><i class=\"fa fa-times tab-list-close\" onclick=\"removeTab('" + tabId + "')\"></i></a></li>";

//         var newTabContent = "<div id=\"" + cTabId + "\" class=\"tab-pane\">";
//         newTabContent += "<div class=\"iframe-block\">";
//         newTabContent += "<div class=\"iframe-body\">";
//         newTabContent += "<iframe id=\"" + fTabId + "\" class=\"iframe_main\" src=\"" + url + "\" height=\"500\" frameborder=\"0\" onload=\"ResizeIframe('" + fTabId + "')\">";
//         newTabContent += "<p>Your browser does not support iframes.</p>";
//         newTabContent += "</iframe>";
//         newTabContent += "</div>";
//         newTabContent += "</div>";
//         newTabContent += "</div>";

//         $("#main-tab-list li").removeClass("active");
//         $("#main-tab-list").append(tabList);

//         $("#main-tab-content").append(newTabContent);
//         $("#" + fTabId).css("min-height", (window.screen.availHeight - 105) + "px");

//         var newElmActive = $("#main-tab-list li#" + hTabId + " a");
//         newElmActive.click();
//     }
// }

// function removeTab(tabId) {
//     var hTabId = "h_" + tabId;
//     var cTabId = "c_" + tabId;

//     $("#" + hTabId).remove();
//     $("#" + cTabId).remove();

//     var elmCount = $("#main-tab-list").children().length;
//     if (elmCount == 0) {
//         defaultTab();
//     }

//     var newElmActive = $("#main-tab-list li:first-child a");
//     newElmActive.click();
// }

$(document).on("shown.bs.tab", ".tab-header", function(e) {
    $("#main-content").css("min-height", window.screen.availHeight + "px");
    var target = $(e.target).attr("href");
    target = target.replace("#c", "f");
    var frame = document.getElementById(target);
    var frameHeight = (frame.contentWindow.document.body.scrollHeight + 105);
    var mainHeight = $("#main-content").height();
    if (frameHeight > mainHeight) {
        $("#main-content").css("min-height", (frame.contentWindow.document.body.scrollHeight + 105) + "px");
    } else {
        $("#main-content").css("min-height", window.screen.availHeight + "px");
    }
});