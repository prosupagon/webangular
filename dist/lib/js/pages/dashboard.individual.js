function LoadDashboardAbsent() {
    Highcharts.setOptions({
        lang: {
            thousandsSep: ','
        }
    });

    $('#absent_bar').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['Late', 'Early', 'Absent'],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Date',
                align: 'high',
                style: {
                    color: "#0e529d",
                    fontWeight: "bold"
                }
            },
            labels: {
                overflow: 'justify'
            },
            tickPositions: [0, 5, 10, 15, 20, 25, 30]
        },
        tooltip: {
            headerFormat: '<span style="font-size: 12px">{point.key}</span> : ',
            pointFormat: '<b>{point.y}</b><br/>',
            valueSuffix: ' day'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true,
                    formatter: function(){
                        console.log(this);
                        var val = this.y;
                        if (val < 0.01) {
                            return '';
                        }
                        return val;
                    }
                }
            }
        },
        legend: {
            enabled: false
        },
        credits : false,
        series: [{
            pointWidth: 35,
            name: 'December 2015',
            data: [
            {
                y: 10,
                name: "Late",
                color: "#70b2e2"
            }, {
                y: 5,
                name: "Early",
                color: "#77ccc7"
            }, {
                y: 2,
                name: "Absent",
                color: "#f36758"
            }
            ]
        }]
    });
}

function LoadDashboardOvertime() {
    Highcharts.setOptions({
        lang: {
            thousandsSep: ','
        }
    });

    $('#overtime_bar').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['1-xxx', '3-xxx', '5-xxx', '7-xxx', '9-xxx', '11-xxx', '13-xxx', '15-xxx', '17-xxx', '19-xxx', '21-xxx', '23-xxx', '25-xxx', '27-xxx', '29-xxx', '31-xxx'],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
            labels: {
                overflow: 'justify',
                format: '{value:.1f}'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }],
            tickInterval: 0.5
        },
        tooltip: {
            headerFormat: '<span style="font-size: 12px">{point.key}</span><br/>',
            valueSuffix: ' hour',
            shared: true
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: false,
                    formatter: function(){
                        console.log(this);
                        var val = this.y;
                        if (val < 0.01) {
                            return '';
                        }
                        return val;
                    }
                }
            }
        },
        legend: {
            enabled: false
        },
        credits : false,
        series: [
            {
                zIndex: 20,
                name: 'overtime',
                color: '#10b8f7',
                data: [2.0, 1.9, 2.5, 4.0, 1.2, 1.5, 2.2, 2.5, 2.3, 1.3, 1.9, 3.6, 2.0, 3.9, 1.5, 1.5],
                dataLabels: {
                    enabled: true
                }
            },
            {
                zIndex: 10,
                name: 'standard',
                color: '#ee1d22',
                data: [2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0],
                marker: {
                    enabled: false,
                    states: {
                        hover: {
                            enabled: false
                        }
                    }
                },
                states: {
                    hover: {
                        lineWidthPlus: 0
                    }
                }
            }
        ]
    });
}

function LoadDashboardTraining() {
    Highcharts.setOptions({
        lang: {
            thousandsSep: ','
        }
    });

    $('#training_bar').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['Target', 'Actual'],
            title: {
                text: null
            }
        },
        yAxis: {
            enabled: false,
            min: 0,
            title: {
                text: ''
            },
            labels: {
                enabled: false
            },
            tickInterval: 10
        },
        tooltip: {
            headerFormat: '<span style="font-size: 12px">{point.key}</span> : ',
            pointFormat: '<b>{point.y}</b><br/>'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true,
                    formatter: function(){
                        console.log(this);
                        var val = this.y;
                        if (val < 0.01) {
                            return '';
                        }
                        return val;
                    }
                }
            }
        },
        legend: {
            enabled: false
        },
        credits : false,
        series: [{
            pointWidth: 35,
            name: 'December 2015',
            data: [
            {
                y: 24,
                name: "Target",
                color: "#77ccc7"
            }, {
                y: 14,
                name: "Actual",
                color: "#f36758"
            }
            ]
        }]
    });
}


function LoadDashboardCompetency() {
    $('#competency_bar').highcharts({

        chart: {
            polar: true,
            type: 'line'
        },

        credits : false,

        title: {
            text: ''
        },

        xAxis: {
            categories: ['Result', 'Leadership', 'Teamwork', 'Expectation', 'Value'],
            tickmarkPlacement: 'on',
            lineWidth: 0
        },

        yAxis: {
            gridLineInterpolation: 'polygon',
            lineWidth: 0,
            min: 0
        },

        tooltip: {
            shared: true,
            headerFormat: '<span style="font-size: 12px">{point.key}</span> : ',
            pointFormat: '<b>{point.y}</b><br/>'
        },

        legend: {
            enabled: false
        },

        series: [{
            name: 'Competency',
            data: [30, 19, 40, 35, 17],
            pointPlacement: 'on'
        }]

    });
}