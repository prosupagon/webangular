/*------------------------------------------------------------------
 [C3 Chart Js Trigger Js]

 Project     :	Fickle Responsive Admin Template
 Version     :	1.0
 Author      : 	AimMateTeam
 URL         :   http://aimmate.com
 Support     :   aimmateteam@gmail.com
 Primary use :	C3 Chart Js use on c3js
 -------------------------------------------------------------------*/

jQuery(document).ready(function($) {
    'use strict';

    c3_bar_chart();

});

function c3_bar_chart(){
    'use strict';

    var chart = c3.generate({
        bindto: '#c3BarCharts', //Here is the selector id
        color: { pattern: [$brownActive,$lightBlueActive, $lightGreen, $blueActive] }, //Here is the color

        data: {
            xs: {
                'ABCD': 'x1',
                'XYZ': 'x2',
                'MNOP': 'x3'
            },
            columns: [ //Here is the data
                ['x1', 2010, 2011, 2012, 2013, 2014],
                ['x2', 2010, 2011, 2012, 2013, 2014],
                ['x3', 2010, 2011, 2012, 2013, 2014],
                ['ABCD', 30, 200, 100, 400, 150, 250],
                ['XYZ', 130, 150, 200, 300, 200, 100],
                ['MNOP', 200, 100, 180, 50, 300, 250]
            ],
            type: 'bar'
        },
        bar: {
            width: {
                ratio: 1 // this makes bar width 50% of length between ticks
            }
        },
        axis: {
            show: true,
            label: {
                text: 'X Label',
                position: 'outer-middle'
            }
        }
    });
}
