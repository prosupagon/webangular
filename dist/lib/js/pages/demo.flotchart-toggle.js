/*------------------------------------------------------------------
 [flotchart  Trigger Js]

 Project     :	Fickle Responsive Admin Template
 Version     :	1.0
 Author      : 	AimMateTeam
 URL         :   http://aimmate.com
 Support     :   aimmateteam@gmail.com
 Primary use :   use on flotchart
 -------------------------------------------------------------------*/

jQuery(document).ready(function($) {
    'use strict';

    plotAccordingToChoicesDataSet(); //Series Toggle Widget chart Load
    plotAccordingToChoicesToggle(); //Series Toggle Widget chart toggle button Trigger
});

/**** Flot Toggle  chart Start ****/
var datasets;
var choiceContainer;
function plotAccordingToChoicesDataSet(){
    'use strict';

    datasets = {
        "usa": {
            label: "USA",
            data: [
                [1988, 483994],
                [1989, 479060],
                [1990, 457648],
                [1991, 401949],
                [1992, 424705],
                [1993, 402375],
                [1994, 377867],
                [1995, 357382],
                [1996, 337946],
                [1997, 336185],
                [1998, 328611],
                [1999, 329421],
                [2000, 342172],
                [2001, 344932],
                [2002, 387303],
                [2003, 440813],
                [2004, 480451],
                [2005, 504638],
                [2006, 528692]
            ]
        },
        "russia": {
            label: "Russia",
            data: [
                [1988, 218000],
                [1989, 203000],
                [1990, 171000],
                [1992, 42500],
                [1993, 37600],
                [1994, 36600],
                [1995, 21700],
                [1996, 19200],
                [1997, 21300],
                [1998, 13600],
                [1999, 14000],
                [2000, 19100],
                [2001, 21300],
                [2002, 23600],
                [2003, 25100],
                [2004, 26100],
                [2005, 31100],
                [2006, 340700]
            ]
        },
        "uk": {
            label: "UK",
            data: [
                [1988, 62982],
                [1989, 62027],
                [1990, 60696],
                [1991, 62348],
                [1992, 58560],
                [1993, 56393],
                [1994, 54579],
                [1995, 50818],
                [1996, 50554],
                [1997, 48276],
                [1998, 47691],
                [1999, 47529],
                [2000, 47778],
                [2001, 48760],
                [2002, 50949],
                [2003, 57452],
                [2004, 60234],
                [2005, 60076],
                [2006, 59213]
            ]
        },
        "germany": {
            label: "Germany",
            data: [
                [1988, 55627],
                [1989, 55475],
                [1990, 58464],
                [1991, 220134],
                [1992, 502436],
                [1993, 47139],
                [1994, 43962],
                [1995, 43238],
                [1996, 42395],
                [1997, 40854],
                [1998, 40993],
                [1999, 41822],
                [2000, 41147],
                [2001, 400474],
                [2002, 40604],
                [2003, 40044],
                [2004, 38816],
                [2005, 38060],
                [2006, 36984]
            ]
        },
        "sweden": {
            label: "Sweden",
            data: [
                [1988, 6402],
                [1989, 6474],
                [1990, 6605],
                [1991, 6209],
                [1992, 6035],
                [1993, 6020],
                [1994, 6000],
                [1995, 6018],
                [1996, 3958],
                [1997, 5780],
                [1998, 5954],
                [1999, 6178],
                [2000, 6411],
                [2001, 5993],
                [2002, 5833],
                [2003, 5791],
                [2004, 5450],
                [2005, 5521],
                [2006, 5271]
            ]
        }

    };
    var i = 0;
    $.each(datasets, function (key, val) {
        val.color = i;
        ++i;
    });

    // insert checkboxes
    choiceContainer = $("#choices");
    $.each(datasets, function (key, val) {
        //<input class="switchCheckBox" type="checkbox" checked data-size="mini">
        choiceContainer.append("<li><input class='switchCheckBox' data-size='mini' type='checkbox' name='" + key +
        "' checked='checked' id='id" + key + "'></input>" +
        "<label class='switch-label' for='id" + key + "'>"
        + val.label + "</label></li>");
    });
    plotAccordingToChoices();
}
function plotAccordingToChoices() {
    'use strict';

    var data = [];
    choiceContainer.find("input:checked").each(function () {
        var key = $(this).attr("name");
        if (key && datasets[key]) {
            data.push(datasets[key]);
        }
    });

    if (data.length > 0) {
        $.plot("#seriesToggle", data, {
            yaxis: {
                min: 0,
                color: '#E3DFD8'
            },
            xaxis: {
                tickDecimals: 0,
                color: '#E3DFD8'
            },
            grid: {
                borderColor: '#E3DFD8'
            },
            colors: [$lightBlueActive, $redActive, $blueActive, $brownActive, $greenActive, $yellowActive]
        });
    }
    $(".switchCheckBox").bootstrapSwitch();
}
function plotAccordingToChoicesToggle(){
    'use strict';

    $(".switchCheckBox").on('switchChange.bootstrapSwitch', function (event, state) {
        plotAccordingToChoices();
    });
}

/**** Flot Toggle  chart End ****/
