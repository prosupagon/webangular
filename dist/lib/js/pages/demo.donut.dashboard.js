/*------------------------------------------------------------------
 [ Profile Trigger Javascript ]

 Project     :	Fickle Responsive Admin Template
 Version     :	1.0
 Author      : 	AimMateTeam
 URL         :  http://aimmate.com
 Support     :  aimmateteam@gmail.com
 Primary use :	Profile

 -------------------------------------------------------------------*/

/*-----------------------------------------------*/
jQuery(document).ready(function($) {
    'use strict';

    morrisCHartStart();
    $(window).resize(function(){
        var windowWidth = $(window).width();
        if(windowWidth < 1025){
            $('#hero-donut').html('');

            clearTimeout(resizeIdMorris);
            resizeIdMorris= setTimeout(morrisCHartStart, 600);
        }

    });

    userGoMap();
});

/*---------------- MORRIS-CHAT-DONUT --------------------*/
function morrisCHartStart(){
    <!--OS Skill-->
    Morris.Donut({
        element: 'hero-donut',
        data: [
            {label: 'OSX', value: 50 },
            {label: 'Linux', value: 60 },
            {label: 'Ubuntu', value: 90 },
            {label: 'Other', value: 10 }
        ],
        formatter: function (y) {
            return y + "%"
        },
        colors: [$fillColor2, $redActive, $greenActive, $lightBlueActive]
    });
}
