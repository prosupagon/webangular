/*------------------------------------------------------------------
 [Sparkline  Trigger Js]

 Project     :	Fickle Responsive Admin Template
 Version     :	1.0
 Author      : 	AimMateTeam
 URL         :   http://aimmate.com
 Support     :   aimmateteam@gmail.com
 Primary use :   use on sparkline
 -------------------------------------------------------------------*/


jQuery(document).ready(function($) {
    'use strict';
    sparkPieChart();
});

function sparkPieChart(){
    $('#sparkPie1').sparkline([1, 3, 4, 8, 5, 2], {
        type: 'pie',
        height: "250px",
        width: "100%",
        sliceColors: [$defultColor, $defultBorder, $lightBlueActive, $redActive, $brownActive, $blueActive],
        borderColor: $defultColor
    });
    $('#sparkPie2').sparkline([7, 3], {
        type: 'pie',
        height: "250px",
        width: "100%",
        sliceColors: [$defultColor, $defultBorder],
        borderColor: $defultColor
    });
}