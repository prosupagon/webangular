var $DashboardManpower = $('#dashboard_manpower');
var pieData = [];
pieData[0] = {
    label: "Man",
    data: Math.floor(Math.random() * 100) + 1
}
pieData[1] = {
    label: "Woman",
    data: Math.floor(Math.random() * 100) + 1
}

function LoadDashboardManpower() {
    'use strict';
    $("#title").text("Custom Label Formatter");
    $("#description").text("Added a semi-transparent background to the labels and a custom labelFormatter function.");
    $.plot($DashboardManpower, pieData, {
        series: {
            pie: {
                show: true,
                radius: 1,
                label: {
                    show: true,
                    radius: 5 / 8,
                    formatter: labelFormatterManpower,
                    background: {
                        opacity: 0
                    }
                }
            }
        },
        legend: {
            show: false
        },
        colors: ['#f36758', '#70b2e2']
    });
}

function labelFormatterManpower(label, series) {
    'use strict';
    return "<div style='font-size:13px; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.data[0][1]) + ", " + Math.round(series.percent) + "%</div>";
}

function LoadDashboardOverAll() {
    Highcharts.setOptions({
        lang: {
            thousandsSep: ','
        }
    });

    $('#over_all_bar').highcharts({
        credits : false,
        title: {
            text: ''
        },
        colors: ['#70b2e2', '#0e529d', '#77ccc7', '#f3582a'],
        xAxis: {
            categories: ['Avg 2014', 'Avg Q1-15', 'Avg Q2-15', 'Avg Q3-15', 'Nov 15', 'Dec 15', 'Plan 2015'],
            labels : {
                rotation: 0,
                style: {
                    fontSize: '10px'
                },
                formatter: function () {
                    var name = this.value['name'];
                    if (typeof name !== "undefined") {
                        name = name.replace(/ /g, '<br />');
                    }
                    return name;
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'normal',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            // enabled: false
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        fontWeight: 'normal',
                        textShadow: '0 0 3px black'
                    },
                    formatter: function(){
                        var val = this.y;
                        if (val < 0.01) {
                            return '';
                        }
                        return val;
                    }
                }
            }
        },
        series: [
            {
                type: 'column',
                name: 'Section 3',
                data: [444, 335, 360, 423, 404, 387, 326],
                // enableMouseTracking: false,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'center'
                }
            }, 
            {
                type: 'column',
                name: 'Section 2',
                data: [1789, 1825, 1782, 1772, 1750, 1813, 1757],
                // enableMouseTracking: false,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'center'
                }
            }, 
            {
                type: 'column',
                name: 'Section 1',
                data: [41, 46, 34, 37, 49, 41, 41],
                // enableMouseTracking: false,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'center',
                    y: -12
                }
            },
            {
                type: 'line',
                name: 'Section 0',
                data: [1500, 1500, 1000, 1000, 1500, 1500, 1500],
                // enableMouseTracking: false,
                marker: {
                    enabled: false,
                    radius: 2
                },
                states: {
                    hover: {
                        lineWidthPlus: 0
                    }
                }
            }
        ]
    });
}

function LoadDashboardOvertime() {
    Highcharts.setOptions({
        lang: {
            thousandsSep: ','
        }
    });

    $('#overtime_bar').highcharts({
        credits : false,
        title: {
            text: ''
        },
        colors: ['#70b2e2', '#f3582a'],
        xAxis: {
            categories: ['Avg 2014', 'Avg Q1-15', 'Avg Q2-15', 'Avg Q3-15', 'Nov 15', 'Dec 15'],
            labels : {
                rotation: 0,
                style: {
                    fontSize: '10px'
                },
                formatter: function () {
                    var name = this.value['name'];
                    if (typeof name !== "undefined") {
                        name = name.replace(/ /g, '<br />');
                    }
                    return name;
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
            stackLabels: {
                enabled: false,
                style: {
                    fontWeight: 'normal',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            // enabled: false
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        fontWeight: 'normal',
                        textShadow: '0 0 3px black'
                    },
                    formatter: function(){
                        var val = this.y;
                        if (val < 0.01) {
                            return '';
                        }
                        return val;
                    }
                }
            }
        },
        series: [
            {
                type: 'column',
                name: 'Section 1',
                data: [84, 85, 79, 87, 84, 84],
                // enableMouseTracking: false,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'center'
                }
            },
            {
                type: 'line',
                name: 'Section 0',
                data: [70, 70, 70, 70, 70, 70],
                // enableMouseTracking: false,
                marker: {
                    enabled: false,
                    radius: 2
                },
                states: {
                    hover: {
                        lineWidthPlus: 0
                    }
                }
            }
        ]
    });

}

function LoadDashboardAbsent() {
    Highcharts.setOptions({
        lang: {
            thousandsSep: ','
        }
    });

    $('#absent_bar').highcharts({
        credits : false,
        title: {
            text: ''
        },
        colors: ['#77ccc7', '#f3582a'],
        xAxis: {
            categories: ['Avg 2014', 'Avg Q1-15', 'Avg Q2-15', 'Avg Q3-15', 'Nov 15', 'Dec 15'],
            labels : {
                rotation: 0,
                style: {
                    fontSize: '10px'
                },
                formatter: function () {
                    var name = this.value['name'];
                    if (typeof name !== "undefined") {
                        name = name.replace(/ /g, '<br />');
                    }
                    return name;
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
            stackLabels: {
                enabled: false,
                style: {
                    fontWeight: 'normal',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            // enabled: false
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        fontWeight: 'normal',
                        textShadow: '0 0 3px black'
                    },
                    formatter: function(){
                        var val = this.y;
                        if (val < 0.01) {
                            return '';
                        }
                        return val;
                    }
                }
            }
        },
        series: [
            {
                type: 'column',
                name: 'Section 1',
                data: [0.51, 1.21, 0.77, 1.11, 0.77, 0.8],
                // enableMouseTracking: false,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'center'
                }
            },
            {
                type: 'line',
                name: 'Section 0',
                data: [1, 1, 1, 1, 1, 1],
                // enableMouseTracking: false,
                marker: {
                    enabled: false,
                    radius: 2
                },
                states: {
                    hover: {
                        lineWidthPlus: 0
                    }
                }
            }
        ]
    });
}

function LoadDashboardTraining() {
    Highcharts.setOptions({
        lang: {
            thousandsSep: ','
        }
    });

    $('#training_bar').highcharts({
        credits : false,
        title: {
            text: ''
        },
        colors: ['#fecf75', '#f3582a'],
        xAxis: {
            categories: [
                {
                    name: "Manager",
                    categories: ["Avg<br/>Q3-15", "Nov 15", "Dec 15"]
                }, {
                    name: "Staff",
                    categories: ["Avg<br/>Q3-15", "Nov 15", "Dec 15"]
                }, {
                    name: "Operator",
                    categories: ["Avg<br/>Q3-15", "Nov 15", "Dec 15"]
                }
            ],
            labels: {
                groupedOptions: [
                    {
                        style: {
                            fontSize: '10px'
                        },
                        y : 14,
                        rotation: 0
                    }
                ],
                style: {
                    fontSize: '8px'
                },
                rotation: 0,
                align: 'center'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'normal',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                },
                formatter: function(){
                    var val = this.total;
                    if (val < 0.01) {
                        return '';
                    }
                    return val;
                }
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            // enabled: false
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: false,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        fontWeight: 'normal',
                        textShadow: '0 0 3px black'
                    },
                    formatter: function(){
                        var val = this.y;
                        if (val < 0.01) {
                            return '';
                        }
                        return val;
                    }
                }
            }
        },
        series: [
            {
                type: 'column',
                name: 'Section 1',
                pointWidth: 25,
                data: [22, 22, 27, 0, 73, 68, 163, 235, 193],
                // enableMouseTracking: false,
                dataLabels: {
                    enabled: false,
                    // rotation: -90,
                    color: '#FFFFFF',
                    align: 'center',
                    formatter: function(){
                        var val = this.y;
                        if (val < 0.01) {
                            return '';
                        }
                        return val;
                    }
                }
            },
            {
                type: 'line',
                name: 'Section 0',
                data: [22, 22, 22, 70, 70, 70, 193, 193, 193],
                // enableMouseTracking: false,
                marker: {
                    enabled: false,
                    radius: 2
                },
                states: {
                    hover: {
                        lineWidthPlus: 0
                    }
                }
            }
        ]
    });
}

function LoadDashboardOverRate() {
    Highcharts.setOptions({
        lang: {
            thousandsSep: ','
        }
    });

    $('#over_rate_bar').highcharts({
        credits : false,
        title: {
            text: ''
        },
        colors: ['#70b2e2', '#77ccc7', '#f3582a'],
        xAxis: {
            categories: ['Jan 15', 'Feb 15', 'Mar 15', 'Apr 15', 'May 15', 'Jun 15', 'Jul 15', 'Aug 15', 'Sep 15', 'Oct 15', 'Nov 15', 'Dec 15'],
            labels: {
                style: {
                    fontSize: '8px'
                },
                rotation: 0,
                align: 'center'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
            stackLabels: {
                enabled: false,
                style: {
                    fontWeight: 'normal',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            // enabled: false
            shared: true
        },
        series: [
            {
                type: 'column',
                name: 'Section 2',
                pointWidth: 12,
                data: [0.44, 0.44, 0.72, 0.44, 0.77, 0.73, 0.49, 0.98, 1.68, 0.05, 0.11, 0.15],
                // enableMouseTracking: false,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: 'gray',
                    align: 'left',
                    style: {
                        fontWeight: 'normal',
                        fontSize: '10px'
                    },
                    formatter: function(){
                        var val = this.y;
                        if (val < 0.01) {
                            return '';
                        }
                        return val;
                    },
                    y: -3
                }
            },
            {
                type: 'column',
                name: 'Section 1',
                pointWidth: 12,
                data: [8.24, 10.72, 10.64, 17.35, 4.58, 3.54, 8.61, 3.31, 2.76, 5.36, 5.76, 3.07],
                // enableMouseTracking: false,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: 'gray',
                    align: 'left',
                    style: {
                        fontWeight: 'normal',
                        fontSize: '10px'
                    },
                    formatter: function(){
                        var val = this.y;
                        if (val < 0.01) {
                            return '';
                        }
                        return val;
                    },
                    y: -3
                }
            },
            {
                type: 'line',
                name: 'Section 0',
                data: [5, 5 ,5, 5, 5 ,5, 5, 5, 5, 5, 5, 5],
                // enableMouseTracking: false,
                marker: {
                    enabled: false,
                    radius: 2
                },
                states: {
                    hover: {
                        lineWidthPlus: 0
                    }
                }
            }
        ]
    });
}

function LoadDashboardWorkshift() {
    Highcharts.setOptions({
        lang: {
            thousandsSep: ','
        }
    });

    $('#workshift_bar').highcharts({
        credits : false,
        title: {
            text: ''
        },
        colors: ['#70b2e2', '#f3582a'],
        xAxis: {
            categories: [
                {
                    name: "2T 2S",
                    categories: ["Avg<br/>Q3-15", "Nov 15", "Dec 15"]
                }, {
                    name: "3T 3S",
                    categories: ["Avg<br/>Q3-15", "Nov 15", "Dec 15"]
                }, {
                    name: "3T 2S",
                    categories: ["Avg<br/>Q3-15", "Nov 15", "Dec 15"]
                }, {
                    name: "Office",
                    categories: ["Avg<br/>Q3-15", "Nov 15", "Dec 15"]
                }
            ],
            labels: {
                groupedOptions: [
                    {
                        style: {
                            fontSize: '10px'
                        },
                        y : 14,
                        rotation: 0,
                    }
                ],
                style: {
                    fontSize: '8px'
                },
                rotation: 0,
                align: 'center'
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
            stackLabels: {
                enabled: false,
                style: {
                    fontWeight: 'normal',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            // enabled: false
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: false,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        fontWeight: 'normal',
                        textShadow: '0 0 3px black'
                    },
                    formatter: function(){
                        var val = this.y;
                        if (val < 0.01) {
                            return '';
                        }
                        return val;
                    }
                }
            }
        },
        series: [
            {
                type: 'column',
                name: 'Section 1',
                pointWidth: 25,
                data: [307, 304, 431, 504, 503, 373, 571, 554, 402, 109, 174, 132],
                // enableMouseTracking: false,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'center'
                }
            },
            {
                type: 'line',
                name: 'Section 0',
                data: [320, 320 ,320, 450, 450 ,450, 450, 450, 450, 130, 130, 130],
                // enableMouseTracking: false,
                marker: {
                    enabled: false,
                    radius: 2
                },
                states: {
                    hover: {
                        lineWidthPlus: 0
                    }
                }
            }
        ]
    });
}