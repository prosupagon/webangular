/*------------------------------------------------------------------
 [Morrisjs  Trigger Js]

 Project     :	Fickle Responsive Admin Template
 Version     :	1.0
 Author      : 	AimMateTeam
 URL         :   http://aimmate.com
 Support     :   aimmateteam@gmail.com
 Primary use :   use on Morrisjs
 -------------------------------------------------------------------*/




jQuery(document).ready(function($) {
    'use strict';
    two_Line_graph();
});

var resizeIdMorris;
$(window).resize(function() {
    clearTimeout(resizeIdMorris);
    resizeIdMorris= setTimeout(doneResizingMorris, 600);

});
function doneResizingMorris(){
    $('#2LineGraph').html('');
    two_Line_graph();
}

var year_data = [
    {"period": "2012", "licensed": 3407, "sorned": -1660},
    {"period": "2011", "licensed": 4351, "sorned": -729},
    {"period": "2010", "licensed": 5269, "sorned": -618},
    {"period": "2009", "licensed": 3246, "sorned": 123},
    {"period": "2008", "licensed": -1257, "sorned": 2667},
    {"period": "2007", "licensed": -3248, "sorned": 1727},
    {"period": "2006", "licensed": 3171, "sorned": -860},
    {"period": "2005", "licensed": 3171, "sorned": -1976},
    {"period": "2004", "licensed": 5201, "sorned": -656},
    {"period": "2003", "licensed": 4215, "sorned": -1022}
];
function two_Line_graph(){
    'use strict';

    Morris.Line({
        element: '2LineGraph',
        data: year_data,
        xkey: 'period',
        ykeys: ['licensed', 'sorned'],
        labels: ['Licensed', 'SORN'],
        lineColors: [$fillColor2, $redActive, $greenActive, $textColor],
        resize: true
    });
}
