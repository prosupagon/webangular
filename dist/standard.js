var app = [{}];

function replace(str, before, after) {
    try{
        return str.replace(new RegExp('{{' + before + '}}', 'g'), after);
    }catch(e){
        return str;
    }
}
app.replace = replace;

function binding(hostID, pattern, dataJson, sort) {
    if(dataJson.length > 0 && typeof pattern == 'string'){
        for(i in dataJson){
            var _pattern = pattern;
            var ovar = dataJson[i];
            var okey = Object.keys(ovar);
            for(j in okey){
                _pattern = app.replace(_pattern, okey[j], ovar[okey[j]]);
            }

            var bindingID = 'bindingID-' + parseInt(Math.random() * 10000);
            _pattern = app.replace(_pattern, '{{bindingID}}', bindingID);

            var _pattern_split = _pattern.split('{{');
            var result = _pattern_split[0];
            for(z = 1, x = _pattern_split.length; z<x; z++){
                result += _pattern_split[z].split('}}')[1];
            }

            var resultObj = $.parseHTML(result);

            if(sort=='prepend')
                hostID.prepend(resultObj);
            else 
                hostID.append(resultObj);

            var bindingObj = hostID;
            for(k in okey){
                bindingObj.find('input[type="radio"][name="' + okey[k] + '"][value="' + ovar[okey[k]] + '"]').attr('checked', true);                
                if(ovar[okey[k]] == true) bindingObj.find('input[type="checkbox"][name="' + okey[k] + '"]').attr('checked', true);
            }    
        }
    }
}
app.binding = binding;

function listAdvance(){
	$('.heading-elements').append($('.datatables-custom-filter'));	
}
app.listAdvance = listAdvance;


function labelValue(val){asd
	var pattern = '<label class="form-control-static">{{Value}}</label>';
	return app.replace(pattern, 'Value', (val==''?'-':val));	
}
app.labelValue = labelValue;

function formView(){	
	$('.form-group > div:nth-child(2) input[type="text"],\
	   .form-group > div:nth-child(2) input[type="number"],\
	   .form-group > div:nth-child(2) select').each(function(){
			$(this).addClass('hidden').after(app.labelValue($(this).val()));		
	});
	
	$('.form-group > div:nth-child(2) input[type="password"]').each(function(){
		$(this)
			.addClass('hidden')
			.after(app.labelValue('********'));		
	});	
}
app.formView = formView;

$(document).on('keyup', '.select2-search__field', function(e) {
	var code = e.keyCode || e.which;
	if(code == 13) {
		if( $(this).parent().next().find('li').eq(0).hasClass('select2-results__message') ) {
			$('.select2.select2-container--open').next().click();
			$('.select2.select2-container--open').prev().select2('close');
		}
	}	
});

function reload(){		
	try{
		$('select:not(.select2-hidden-accessible)').each(function(){
			if( $(this).children().length > 10)
				$(this).select2();
			else
				$(this).select2({ 
					minimumResultsForSearch : -1 
				});
		});
	}catch(e){
		console.warn('Dont have select2.');
	}
	try{
		$('.datetimepicker').attr('placeholder', '__/__/____ __:__')
		$('.datetimepicker').datetimepicker({ 
			format:'d/m/Y H:i',
			mask:true
		});
		$('.datepicker').attr('placeholder', '__/__/____')
		$('.datepicker').datetimepicker({ 
			timepicker:false,
			format:'d/m/Y',
			mask:true
		});
		$('.timepicker').attr('placeholder', '__:__')
		$('.timepicker').datetimepicker({ 
			datepicker:false,
			format:'H:i',
			mask:true
		});
	}catch(e){
		console.warn('Dont have datetimepicker');
	}
	
	currency();
	
	$('.datetimepicker:not(.generated), .datepicker:not(.generated)').each(function(){
		$(this).parent().append('<i class="input-feedback toggle-prev fa fa-calendar"></i>');
	});
	$('.timepicker:not(.generated)').each(function(){
		$(this).parent().append('<i class="input-feedback toggle-prev fa fa-clock-o"></i>');
	});	
	
    $('.datetimepicker:not(.generated), .datepicker:not(.generated), .timepicker:not(.generated), select[multiple]:not(.generated)').each(function(){
		if(!$(this).is(':disabled'))
			$(this).parent().append('<i class="input-feedback toggle-clear fa fa-times"></i>');			
		$(this).addClass('generated')
	});
	
	$('.select2').each(function(){
		$(this).parent().append($(this));		
	});
	
    $('input[type="checkbox"]:not(.checkboxev-parent)').each(checkboxev);
    $('input[type="radio"]:not(.radioev-parent)').each(radioev);
}
app.reload = reload;
$(document).ready(app.reload);
$(document).on('click', '.toggle-prev', function(){
	$(this).siblings('input').focus();	
});
$(document).on('click', '.toggle-clear', function(){
	$(this).siblings('input').val('');
	$(this).siblings('select[multiple]').val([]);	
	$(this).siblings('select[multiple]').parent().find('li.select2-selection__choice').remove();
});

$(document).on('click', '.checkboxev:not([disabled])', function () {
    var parentCheckbox = $(this).prev();
    if (parentCheckbox.prop('checked')) {
        $(this).attr('class', 'checkboxev glyphicon glyphicon-unchecked')
        parentCheckbox.click();
    } else {
        parentCheckbox.click();
        $(this).attr('class', 'checkboxev glyphicon glyphicon-check')
    }
});
function checkboxev() {
    $(this)
        .addClass('hidden')
        .addClass('checkboxev-parent');
    if ($(this).attr('checked'))
        if($(this).is('[disabled]'))
            $(this).after('<i class="checkboxev glyphicon glyphicon-check" disabled></i>');
        else
            $(this).after('<i class="checkboxev glyphicon glyphicon-check"></i>');
    else
        if($(this).is('[disabled]'))
            $(this).after('<i class="checkboxev glyphicon glyphicon-unchecked" disabled></i>');
        else
            $(this).after('<i class="checkboxev glyphicon glyphicon-unchecked"></i>');
}

$(document).on('click', '.radioev:not([disabled])', function () {
    var parentCheckbox = $(this).prev();
    $('[data-radioev="' + $(this).data('radioev') + '"]').attr('class', 'radioev fa fa-circle-o');
    //parentCheckbox.prop("checked", true);
    parentCheckbox.click();
    $(this).attr('class', 'radioev fa fa-dot-circle-o');
});
function radioev() {
    $(this)
        .addClass('radioev-parent')
        .addClass('hidden');
    if ($(this).attr('checked'))
        if($(this).is('[disabled]'))
            $(this).after('<i data-radioev="' + $(this).attr('name') + '" class="radioev fa fa-dot-circle-o" disabled></i>');
        else 
            $(this).after('<i data-radioev="' + $(this).attr('name') + '" class="radioev fa fa-dot-circle-o"></i>');
    else
        if($(this).is('[disabled]'))
            $(this).after('<i data-radioev="' + $(this).attr('name') + '" class="radioev fa fa-circle-o" disabled></i>');
        else
            $(this).after('<i data-radioev="' + $(this).attr('name') + '" class="radioev fa fa-circle-o"></i>');
}

function currency(){	
	$(document).on('blur', '.currency', function(){  
		this.value = parseFloat(this.value.replace(/,/g, ""))
			.toFixed(2)
			.toString()
			.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	});
}

/*
function reload(){	
	$('.datetimepicker, .datepicker').each(function(){
		$(this).parent().append('<i class="input-feedback fa fa-calendar"></i>');		
	});
	$('.timepicker').each(function(){
		$(this).parent().append('<i class="input-feedback fa fa-clock-o"></i>');		
	});
	
    $('input[type="radio"]:not(.radioev-parent)').each(radioev);
    $('input[type="checkbox"]:not(.checkboxev-parent)').each(checkboxev);
    $('input.inputPassword').each(parsePassword);
    $('input[type="password"]').each(inputPassword);
    $('input[placeholder="Username"]').each(inputUsername);	
    
    $('select:not([multiple])').each(function(){
        if($(this).children().length > 10)
            $('select').select2();        
        else
            $('select').select2({
                minimumResultsForSearch: -1
            });
    });
}
app.reload = reload;

$(document).ready(function(){	
	app.reload();
});
*/






















































