// Data lookup mockup
var datalookup = {};
datalookup.status = [{
		'title': 'Active',
		'value': '1',
	},{
		'title': 'Inactive',
		'value': '0',
	}];
datalookup.gender = [{
		'title': 'Male',
		'value': 'm',
	},{
		'title': 'Female',
		'value': 'f',
	}];




$(document).ready(function(){

});

var allGroup = [];
var dataExcept = ['uiSortableItem','sortableItem',"uiSortable",'class'];
var dataChoice = ['checkbox','radiobox','dropdown'];
var template = {
	rowtitle: $('#template-rowtitle').html(),
	textbox: $('#template-textbox').html(),
	password: $('#template-password').html(),
	number: $('#template-number').html(),
	textarea: $('#template-textarea').html(),
	checkbox: $('#template-checkbox').html(),
	radiobox: $('#template-radiobox').html(),
	calendarpicker: $('#template-calendarpicker').html(),
	'switch': $('#template-switch').html(),
	dropdown: $('#template-dropdown').html(),
	fileupload: $('#template-fileupload').html(),
	table: $('#template-table').html()
}
var listitem = {
	drag: $('#listitem-drag').html()
}
var propertyitem = {
	option: $('#property-option').html(),
	textbox: $('#property-textbox').html(),
	password: $('#property-textbox').html(),
	checkbox: $('#property-checkbox').html(),
	radiobox: $('#property-radiobox').html(),
	dropdown: $('#property-dropdown').html()
}


var option = {};
option.drag = [{
		title: 'Text',
		control: 'textbox',
		property: 'text'
	},{
		title: 'Email',
		control: 'textbox',
		property: 'email'
	},{
		title: 'Web address',
		control: 'textbox',
		property: 'webaddress'
	},{
		title: 'Phone',
		control: 'textbox',
		property: 'phone'
	},{
		title: 'Mobile',
		control: 'textbox',
		property: 'mobile'
	},{
		title: 'Password',
		control: 'password',
		property: 'password'
	},{
		title: 'Number',
		control: 'number',
		property: 'number'
	},{
		title: 'Currency',
		control: 'number',
		property: 'currency'
	},{
		title: 'Textarea',
		control: 'textarea',
		property: 'textarea'
	},{
		title: 'HTMLEditor',
		control: 'textarea',
		property: 'htmleditor'
	},{
		title: 'Checkbox',
		control: 'checkbox',
		property: 'manual'
	},{
		title: 'Checkbox Data',
		control: 'checkbox',
		property: 'datalookup'
	},{
		title: 'Radiobox',
		control: 'radiobox',
		property: 'manual'
	},{
		title: 'Radiobox Data',
		control: 'radiobox',
		property: 'datalookup'
	},{
		title: 'Date picker',
		control: 'calendarpicker',
		property: 'datepicker'
	},{
		title: 'Time picker',
		control: 'calendarpicker',
		property: 'timepicker'
	},{
		title: 'Date time picker',
		control: 'calendarpicker',
		property: 'datetimepicker'
	},{
		title: 'Switch',
		control: 'switch',
		property: 'switch'
	},{
		title: 'Dropdown',
		control: 'dropdown',
		property: 'manual'
	},{
		title: 'Dropdown Data',
		control: 'dropdown',
		property: 'datalookup'
	},{
		title: 'Dropdown Search',
		control: 'dropdown',
		property: 'filtersearch'
	},{
		title: 'Fileupload',
		control: 'fileupload',
		property: 'fileupload'
	},{
		title: 'Table',
		control: 'table',
		property: 'table'
	}];

option.control = []
for(i in option.drag){
	var optioncontrol = {
		title: option.drag[i].title,
		property: option.drag[i].control
	}
	option.control.push(optioncontrol);
}
	
app.binding($('#menuleft .listitem [data-menu="layout"]'), listitem.drag, option.drag);
app.binding($('#menuright [data-ddl="control"]'), propertyitem.textbox, option.control);
app.reload();

function getAllGroup(){
	$.get('/servicebuilder/index.php?r=api/Servicebuilder', function(e){
		var jsonAllGroup = setJsonData(e, {'id':'value', 'name':'title'});
			
		//console.log(jsonAllGroup);
		app.binding($('#menuright [data-ddl="groupover"]'), propertyitem.option, jsonAllGroup);
		app.reload();
	});
}
getAllGroup();

function setJsonData(datajson, config){
	var configKey = Object.keys(config)
	for(i in datajson){		
		for(j in configKey){
			datajson[i][config[configKey[j]]] = datajson[i][configKey[j]];
		}
	}

	return datajson;
}

function optionDataLookup(){
	var lookupKey = Object.keys(datalookup)
	var lookupData = [];
	for(i in lookupKey){
		var j = {
			title: lookupKey[i],
			value: lookupKey[i]
		}
		lookupData.push(j)
	}

	app.binding($('#menuright [data-ddl="lookup"]'), propertyitem.option, lookupData);
	app.reload();
}
optionDataLookup();

function optionBycontrol(ctrl){
	var optionCtrl = [];
	for(i in option.drag){
		if(option.drag[i].control == ctrl){
			optionCtrl.push(option.drag[i]);
		}
	}
	return optionCtrl;
}
	
var droppable = true; // For fix bug doubble droppable
$( ".screen.custom" ).droppable({
     drop: function(event, ui) {
		if(droppable){
			droppable = false;
			setTimeout(function(){
				workspace();
				loadform($(this));
				var dropped = $(ui.draggable);								
				if( dropped.hasClass('layout-row') && dropped.children('.rowtitle').length == 0 ){
					genrowtitle(dropped);
				}
				droppable = true;
			}, 10);
		}
	}
});

function genrowtitle(obj){
	app.binding(obj, template.rowtitle, [{ title: obj.data('title') }]);
}

function loadform(host)
{
	if(!host.data('id')){
		var uniqueId = uniqueID();
		host.attr('data-id', uniqueId);
		host.data('id', uniqueId);
		host.attr('data-split', 'example 1, example 2');
		host.data('split', 'example 1, example 2');
	}
	
	if( dataChoice.indexOf(host.data('control')) != -1 ){
		if(!host.data('control')){
			host.attr('data-choice', 1);
			host.data('choice', 1);
			host.attr('data-lookup', '');
			host.data('lookup', '');
		}
	}

	host.find('button').remove();
	host.find('ul > *:not(li)').remove();
	host.find('ul').each(function(){
		genrowtitle($(this));
	});

	host.find('li').removeClass('loadform').empty();
	host.find('li:not(.loadform)').each(function(){
		geninput($(this));
	});
	
	if(host.find('.modal-footer').length > 0)
		host.find('.modal-footer').append('<button type="submit" class="btn btn-sm btn-primary pull-right submit">Submit</button>');
	else
		host.append('<button type="submit" class="btn btn-sm btn-primary pull-right submit">Submit</button>');
		
	groupover();
}
/*
function loadform(){
	$('#workspace ul, #workspace li').each(function(){
		if(!$(this).data('id')){
			var uniqueId = uniqueID();
			$(this).attr('data-id', uniqueId);
			$(this).data('id', uniqueId);
			$(this).attr('data-split', 'example 1, example 2');
			$(this).data('split', 'example 1, example 2');
		}
		
		if( dataChoice.indexOf($(this).data('control')) != -1 ){
			if(!$(this).data('control')){
				$(this).attr('data-choice', 1);
				$(this).data('choice', 1);
				$(this).attr('data-lookup', '');
				$(this).data('lookup', '');
			}
		}
	});

	$('.screen ul > *:not(li)').remove();
	$('.screen ul').each(function(){
		genrowtitle($(this));
	});

	$('.screen li').removeClass('loadform').empty();
	$('.screen li:not(.loadform)').each(function(){
		geninput($(this));
	});
}
*/
//loadform();

function uniqueID(){
	var randomId = 'id' + (parseInt(Math.random() * 90000) + 10000);
	var result;	
	if($(document).find('[data-id="' + randomId + '"]').length == 0) 
		result = randomId;
	else 
		result = uniqueID();			
	return result;
}

function geninput(obj){	
	obj.empty();
	obj.addClass('loadform');
	var objData = getAttrData(obj);
	if(objData.Data.control && template[objData.Data.control]){	
		app.binding(obj, template[objData.Data.control], [objData.Data]);
	}
	
	if( dataChoice.indexOf(obj.data('control')) != -1 ){	
		var controlItems = choiceSplit(obj);
		if(obj.data('choice') == 2 && obj.data('lookup')){	
			controlItems = datalookup[obj.data('lookup')];
		}
		
		if(obj.data('control') != 'dropdown'){
			app.binding(obj.find('.control ul'), propertyitem[obj.data('control')], controlItems);
			if(obj.data('control') == 'radiobox'){				
				obj.find('input').first().attr('checked', 'checked');
			}
		}else{
			app.binding(obj.find('.control select'), propertyitem[obj.data('control')], controlItems);
		}
	}
	
	app.reload();
}

function choiceSplit(obj){
	var choice = obj.data('split').split(',');
	var result = [];
	for(i in choice){
		var item = {
			name: obj.data('id'),
			value: (parseInt(i) + 1),
			title: choice[i]
		}
		
		result.push(item);
	}
	return result;
}

workspace();
$(document).find(".screen.custom").sortable();
$(document).find("#menuleft li[data-layout]").draggable({
	connectToSortable: ".layout-row",
	helper: "clone",
	revert: "invalid"
});	

$(document).find("#menuleft .layout-row").draggable({
	connectToSortable: ".screen",
	helper: "clone",
	revert: "invalid"
});

function workspace(){
	$(document).find(".screen.custom .layout-row").sortable({
		items: "> li"
	});	
	$("ul, li" ).disableSelection();	
}

var dataSelected = false;
$(document).on('click', '.rowtitle .option .edit', function(){
	$('.layout-row, li[data-layout]').removeClass('active');
	layoutrow = $(this).parents('ul').first();
	layoutrow.addClass('active');	
	
	dataSelected = $(this).parents('ul').first();
	var attrDataSelected = getAttrData(dataSelected);	
	$('#menuright [data-text="title"]').val(dataSelected.data('title'));
	$('#information').removeClass('hidden');
});

$(document).on('mousedown', '.screen.custom li[data-layout]', function(){	
	$('.layout-row, li[data-layout]').removeClass('active');
	$(this).addClass('active');
	dataSelected = $(this);
	
	$('[data-anycontrol]').addClass('hidden');
	$('[data-anycontrol]').each(function(){
		var anycontrols = $(this).data('anycontrol').split(',');
		for(i in anycontrols){
			if(dataSelected.data().control == anycontrols[i])
				$(this).removeClass('hidden');
		}
	});	
	
	setinfo();
	$('#information').removeClass('hidden');
	
	//console.log( dataSelected.data() );
});

function setinfo(){	
	//info-property
	var attrDataSelected = getAttrData(dataSelected);
	
	try{
		$('#info-property').select2('destroy');
	}catch(e){
	
	}
	
	$('#info-property').empty();	
	app.binding($('#info-property'), propertyitem.option, optionBycontrol(attrDataSelected.Data.control));
	app.reload();
		
	for(i in attrDataSelected.Key){
		$('[data-ddl="' + attrDataSelected.Key[i] + '"]').val(dataSelected.data(attrDataSelected.Key[i])).change();
	}
	
	$('#menuright select:not([data-ddl="layout"])').each(function(){
		var thisVal = dataSelected.data($(this).data('ddl')) || false;
		if(thisVal)
			$(this).val(thisVal).change();
		else
			$(this).val($(this).children('option').first().val()).change();
	});
	$('#menuright [data-text]').each(function(){
		var thisVal = dataSelected.data($(this).data('text')) || '';
		$(this).val(thisVal);
	});
}

$('#menuright [data-text]').keyup(function(){
	dataSelected.attr('data-' + $(this).data('text'), $(this).val());
	dataSelected.data($(this).data('text'), $(this).val());
	if(dataSelected.is('ul'))
		dataSelected.find('.rowtitle strong').text($(this).val());
	else
		geninput(dataSelected);
});

function getAttrData(host){
	var hostDataRaw = $(host).data();
	var hostDataKey = Object.keys(hostDataRaw);
	var hostData = { Data:{}, Key:[] };
	for(i in hostDataKey){
		if(dataExcept.indexOf(hostDataKey[i]) == -1){
			hostData.Data[hostDataKey[i]] = hostDataRaw[hostDataKey[i]];
			hostData.Key.push(hostDataKey[i]);
		}
	}
	return hostData;
}

$('#layout-layout').change(function(){
	if(dataSelected.hasClass('active')){
		dataSelected.attr('class', $(this).val() + ' ui-draggable ui-draggable-handle active');
	}else{
		dataSelected.attr('class', $(this).val() + ' ui-draggable ui-draggable-handle');
	}
});

$('[data-ddl]').change(function(){
	dataSelected.attr('data-' + $(this).data('ddl'), $(this).val());
	dataSelected.data($(this).data('ddl'), $(this).val());
	geninput(dataSelected);
});

$('#menuright [data-ddl="choice"]').change(function(){
	$('.choicedata').addClass('hidden');
	$('.choicedata[data-choice="' + $(this).val() + '"]').removeClass('hidden');
});

$('#clearinfo').click(function(){
	if(confirm("Delete this column ?")){
		dataSelected.remove();
		$('#information').addClass('hidden');
	}
});

$(document).on('click', '.screen.custom .rowtitle .option .delete', function(){
	if(confirm("Delete this row ?"))
		$(this).parents('ul').first().remove();
});

$('#savegroup').click(function(e){
	e.preventDefault();
	var dataSend = {};
	$('.data').each(function(){
		dataSend[$(this).attr('id')] = $(this).val();
	});

	var dataString = $('.screen').clone();
	dataString.find('li').empty();
	dataString.find('ul > *:not(li)').remove();
	dataString.find('ul, li').removeAttr('style');
	dataString.find('*').removeClass('ui-draggable ui-draggable-handle ui-sortable loadform active');
	dataString = dataString[0].innerHTML.trim().replace(/(\r\n|\n|\r)/gm,"");
	dataSend.element = dataString;
	//console.log(dataSend.element);		
	//console.log(dataSend);
	//console.log(JSON.stringify(dataSend));
	//sendPostView(dataSend);
});

function clearScreen()
{
	var screen = $('.screen');
	screen.find('li').empty();
	screen.find('ul > *:not(li)').remove();
	screen.find('ul, li').removeAttr('style');
	screen.find('*').removeClass('ui-draggable ui-draggable-handle ui-sortable loadform active');
}

function sendPostView(data)
{
	//console.log(data);
	$('#raw').text(data.element);
	setTimeout(function(){
		$('form').submit();
	}, 100);	
}

$('.menulist [data-tab]').click(function()
{
	$('.menulist [data-tab]').removeClass('active');
	$(this).addClass('active');
	$('.listitem [data-tab]').removeClass('active');
	$('.listitem [data-tab="' + $(this).data('tab') + '"]').addClass('active');
});
$('.menulist [data-tab]').first().click();

function bindingForm(host, data){
	host.find('.radioev').remove();
	host.find('*').removeClass('radioev-parent');
	
	var dayaKeys = Object.keys(data);
	for(i in dayaKeys){
		var obj = $('li[data-id="' + dayaKeys[i] + '"]');
		if(obj.length){
			$('li[data-id="' + dayaKeys[i] + '"]').addClass('binded');
			$('li[data-id="' + dayaKeys[i] + '"] .dataview').text(data[dayaKeys[i]]);
					
			switch(obj.data('control')){
				case 'radiobox':
					$('li[data-id="' + dayaKeys[i] + '"] li > input').removeAttr('checked');			
					$('li[data-id="' + dayaKeys[i] + '"] li > input[value="' + data[dayaKeys[i]] + '"]').attr('checked', true);			
					$('li[data-id="' + dayaKeys[i] + '"] li > input:not([checked])').parent().remove();
					$('li[data-id="' + dayaKeys[i] + '"] li > :not(label)').remove();
					$('li[data-id="' + dayaKeys[i] + '"] .dataview').remove();
					break;
				case 'checkbox':
					for(j in data[dayaKeys[i]]){
						$('li[data-id="' + dayaKeys[i] + '"] li > input[value="' + data[dayaKeys[i]][j] + '"]').next().click();
					}
					$('li[data-id="' + dayaKeys[i] + '"] li > input:not(:checked)').parent().remove();
					$('li[data-id="' + dayaKeys[i] + '"] li > :not(label)').remove();
					$('li[data-id="' + dayaKeys[i] + '"] .dataview').remove();
					break;
				case 'dropdown':
					$('li[data-id="' + dayaKeys[i] + '"] .control > select').val(data[dayaKeys[i]]).change();
					var valueCaption = $('li[data-id="' + dayaKeys[i] + '"] .control > select option[value="' + data[dayaKeys[i]] + '"]').text();
					$('li[data-id="' + dayaKeys[i] + '"] .dataview').text(valueCaption);
					$('li[data-id="' + dayaKeys[i] + '"] .control :not(.dataview)').remove();
					break;
			}
		}
	}			
	$('ul > li:not(.binded) .control :not(.dataview)').remove();
}


$(document).on('click', '.control label', function()
{
	$(this).prev().click();
});


/* API Groupcontrol */
$('[data-groupcontrol]:not(.loadded)').each(function()
{
	$(this).addClass('loadded');
	var that = $(this);	
	$.get('/servicebuilder/index.php?r=api/Servicebuilder&id=' + $(this).data('groupcontrol') , function(e){
		that.append(e.raw);
		loadform(that);
		try{
			if(dataJson) bindingForm($('#customform'), dataJson);
		}catch(e){}
	});
});


/* API Groupover */
var modalAdditem;
$(document).on('mousedown', '[data-target="#modal-additem"]', function()
{
	modalAdditem = $(this);
	var modalAddItemRaw = $('#modal-additem');
	$.get('/servicebuilder/index.php?r=api/Servicebuilder&id=' + $(this).data('groupover') , function(e){
		modalAddItemRaw.find('.raw').empty().append(e.raw);
		modalAddItemRaw.find('.modal-title').text(e.name);
		loadform(modalAddItemRaw);
	});
});


/* Api Table builder */
function groupover()
{
	$('.loadform[data-groupover][data-control="table"]').each(function(){		
		var that = $(this);
		that.removeClass('loadform');
		
		$.get('/servicebuilder/index.php?r=api/Servicebuilder&id=' + that.data('groupover') , function(e){
			that.find('table thead, table tbody').empty();
			that.find('table thead').append('<tr></tr>');
			var thatTheadTr = that.find('table thead tr');
			var thatTbody = that.find('table tbody');
			var thatTheadData = [];
			$(e.raw).find('[data-id]').each(function(){
				thatTheadTr.append('<td>' + $(this).data('title') + '</td>');
				thatTheadData.push($(this).data('id'));
			});
			$.get('/servicebuilder/index.php?r=api/Servicebuilder&form_id=' + that.data('groupover') + '&parent_id=' + that.parents('[data-groupcontrol]').data('id') , function(e2){				
				for(i in e2){
					var item = $('<tr></tr>'); 
					var j = JSON.parse(e2[i].data);
					for(k in thatTheadData){
						item.append('<td>' + j[thatTheadData[k]] + '</td>');
					}					
					thatTbody.append(item);
				}
			});
		});
	});
}


/* Api Groupover additem */
$(document).on('click', '#modal-additem .submit', function()
{	
	var dataSend = {};
	dataSend.form_id = modalAdditem.data('groupover');
	dataSend.parent_id = modalAdditem.parents('[data-groupcontrol]').data('groupcontrol');;
		
	modalData = '{';
	$(document).find('#modal-additem [name]').each(function(){
		modalData += '"' + $(this).parents('[data-id]').first().data('id') + '":"' + $(this).val() + '",';
	});
	modalData = modalData.substring(0, modalData.length - 1) + '}';
	dataSend.data = modalData;
	
	//console.log(dataSend);
	
	$.post('/servicebuilder/index.php?r=api/additem', dataSend, function(e){
		console.log(e);
	});
});































