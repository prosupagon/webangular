$(document).ready(function(){
    var orgchartMethod = "member";

	var getlocation = window.location.search.substr(1);
	//console.log(getlocation);
	var params;
	function getParams(query) {
		var i = [];
		query = query.split(",");
		query.forEach(function (v) {
			var j = v.split('=');
			i[j[0]] = j[1];
		});
		return i;
	}
	params = getParams(getlocation);
	//console.log(params)

	var layouts = [];
	layouts[1] = 'step';
	layouts[2] = 'horizontal';
	layouts[3] = 'vertical';

	var orgchart_layout = parseInt(params.layout);
	if(layouts.indexOf(orgchart_layout) == -1)
		orgchart_layout = 3;
	
	if(orgchart_layout){
		$('.orgchart').addClass('orgchart-' + layouts[orgchart_layout]);
	}else{
		$('.orgchart').addClass('orgchart-' + layouts[1]);
	}

	var currentId = 0;
	var tree = [0];
	var branch = 0;
	function addItem(obj){
		console.log(obj);		
		if(obj.ActionByOID == ""){
			branch = currentId;
		}
		if(obj.ActionByOID != ""){
			
			var parentOID = currentId++;
			if(currentId == 10)
				parentOID = 3;
						
			var card = '';    
			//console.log(obj);
			
				card = '' +
				'<div class="card card-group id-' + currentId + ' parent-' + parentOID + ' type-dept">' +
			'        <span class="after"></span>' +
			'        <span class="before"></span>' +
				'    <div class="detail group" data-type="dept">' +
				'        <span class="after"></span>' +
				'        <span class="before"></span>' +
				'        <button data-orgid="' + currentId + '">' +
				'            <h3>' + obj.StepName + '</h3>' +
				'            <span class="after"></span>' +
				'            <p class="actionby">' + obj.ActionByName + '</p>' +
				'            <p class="createdate">' + obj.CreateDate + '</p>' +
				'            <p class="duration hidden">16days 6hrs 32min</p>' +
				'        </button>' +
				'        <div class="parent"></div>' +
				'    </div>' +
				'    <div class="children"></div>' +
				'</div>';
			
				/*
				'<div class="card card-group id-' + obj.Id + ' deep-' + tree.indexOf(obj.ParentId) + ' parent-' + obj.ParentId + ' type-' + obj.Type + '">' +
				'    <div class="detail group" data-type="' + obj.Type + '">' +
				'        <button data-orgid="' + obj.Id + '">' +
				'            <h3>Receive</h3>' +
				'            <p>System Administrator</p>' +
				'            <p>2017-01-10 15:03</p>' +
				'            <p class="duration">16days 6hrs 32min</p>' +
				'        </button>' +
				'        <div class="parent"></div>' +
				'    </div>' +
				'    <div class="children"></div>' +
				'</div>';
				*/
				
			$('.id-' + parentOID + '.type-dept > .children').each(function(){
				$(this).append(card);
			});
		}
		if(obj.ActionByOID == ""){
			branch = currentId;
		}
	}


	//brotherline
	brotherline();
	function brotherline() {
		$('.card').each(function () {
			//console.log($(this).nextAll('.card').length);
			if ($(this).nextAll('.card').length > 0) {
				$(this).addClass('brother');
			}
		});
		
		$('.type-dept').each(function(){
			//console.log("branch", $(this));
			if($(this).children('.children').children('.card').length == 0)
				$(this).addClass('empty');
			else
				$(this).removeClass('empty');
				
			$(this).attr('mychild-amount', $(this).children('.children').children('.card').length);
		}); 
	}



	$('#opens').click(function(){     
		$('.detail.group').each(function(){
			$(this).parent().addClass('branch');
		});
		fitscreen();
	});


	//parent
	$('.parent:not([ready])').click(pinchart);
	$('.parent').attr('ready', true);
	function pinchart(){
		$('#page').addClass('hidden');
		
		var clonecard = $(this).parent().parent();
		$('#pin > .children').empty()
		$('#pin > .children').html(clonecard.clone());
		$('#pin > .children > .card').removeClass('brother');
		
		$('#pin .parent').click(pinchart);
		$('#pin > .children > .card > .detail .parent').click(function(){
			$('#pin > .children').empty();
			$('#page').removeClass('hidden');
		});
		
		setTimeout(function(){
			$('#opens').click();
		}, 100);    
	}




	var detail_wmax = 130;
	function resizecard() {
		$('.detail strong, .detail span').each(function () {
			if ($(this).width() > detail_wmax)
				fitcol($(this));
		});
	}
	function fitcol(obj){
		obj.css('font-size', (parseInt(obj.css('font-size')) - 1) + 'px');
		if(obj.width() > detail_wmax)
			fitcol(obj);        
	}


	//cardcontrol();
	function cardcontrol(){
		$(document).on('click', '.detail.group', function(){ 
			//console.log($(this).data('type'));	
			//$(this).parent().toggleClass('branch');

			var meTypeid = $(this).data('type');
			var meId = $(this).children('button').data('orgid');
			
			$('[data-type="' + meTypeid + '"] [data-orgid="' + meId + '"]').each(function(){
				$(this).parent().parent().toggleClass('branch');
			});
			
			/* ใช้งานในกรณีที่ต้องการแยกโหลดตาม department
			if(!$(this).attr('loaddata')){            
				if(!params.overload){
					$('[data-type="' + meTypeid + '"] [data-orgid="' + meId + '"]').each(function(){
						$(this).parent().attr('loaddata', true);
						$(this).append('<i class="loader"></i>');
					});
					getOrganize(meTypeid, meId);	
				}	
			}		
						
			fitscreen();
			*/
		}); 
	}

		
		
	$('#print').click(function () {
		//fitscreen();
		resetscreen();
		printscreen();
		setTimeout(function(){
			window.print();
			releasescene();
			resetscreen();
		}, 100);
	});

	$('#overload').click(function(){
		
		if(confirm("อาจใช้เวลาในการดำเนินการเป็นเวลานาน คุณต้องการดูข้อมูลทั้งหมดหรือไม่?")){
			$('#overloading').addClass('active');
			
			if( $('#page').hasClass('hidden') ){
				var orgchart_dept = $('#pin .card > .detail').eq(0).children('button').data('orgid');
				window.location.href = window.location.href + '?overload=1,layout=' + orgchart_layout + ',dept=' + orgchart_dept;
			}else
				window.location.href = window.location.href + '?overload=1,layout=' + orgchart_layout;
		}
	});

	getOrganizeOverload();
	function getOrganizeOverload(){
		var _orgchartMethod = 'member';
		if(orgchartMethod)
			_orgchartMethod = orgchartMethod;
		
		//var requestApi = window.location.href + '?method=' + _orgchartMethod;
		var dumpJson1 = [{"Id":"9999999","ParentId":"0","Name":"Organization 2016","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Calendar-icon.png","CountChild":"","Type":"dept","ParentType":"org"},{"Id":"1","ParentId":"9999999","Name":"ENKEI THAI CO., LTD.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"1","Type":"dept","ParentType":"dept"},{"Id":"2","ParentId":"1","Name":"MANAGEMENT COMMITTEE","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"3","ParentId":"1","Name":"MANAGING DIRECTOR","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"4","ParentId":"1","Name":"SINIOR  ADVISOR","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"5","ParentId":"1","Name":"QUALITY MANAGEMENT REPRESENTATIVE","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"6","ParentId":"1","Name":"ENVIRONTMENT MANAGEMENT REPRESENTATIVE","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"7","ParentId":"1","Name":"NULLOCCUPATIONAL HEALTH AND SAFTY MANAGEMENT REPRESENTATIVE","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"8","ParentId":"1","Name":"SOCIAL ACCOUNTABILITY  MANAGEMENT REPRESENTATIVE","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"9","ParentId":"1","Name":"INTERNAL AUDIT","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"10","ParentId":"3","Name":"CORPORATE PLANNING","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"17","ParentId":"3","Name":"SALES & MARKETING","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"21","ParentId":"3","Name":"QUALITY ASSURANCE","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"15","ParentId":"3","Name":"EPS. SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"16","ParentId":"3","Name":"SAFETY & OH. SEC. ","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"29","ParentId":"3","Name":"FINANCE","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"32","ParentId":"3","Name":"ACCOUNTING","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"36","ParentId":"3","Name":"PURCHASING & W/H","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"42","ParentId":"3","Name":"ADMINISTRATION ","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"53","ParentId":"3","Name":"PRODUCTION CONTROL","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"60","ParentId":"3","Name":"PRODUCTION PLANT ","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"155","ParentId":"3","Name":"PRODUCTION  MANAGEMENT","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"160","ParentId":"3","Name":"FOUNDRY GROUP","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"181","ParentId":"3","Name":"ENGINEERING","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"11","ParentId":"10","Name":"CORPORATE PLANNING DEPT.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"12","ParentId":"11","Name":"CORPORATE PLANNING SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"13","ParentId":"11","Name":"IT.SEC","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"14","ParentId":"11","Name":"IFA SEC","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"18","ParentId":"17","Name":"SALES & MARKETING DEPT.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"19","ParentId":"18","Name":"SALES & MARKETING 1 SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"20","ParentId":"18","Name":"SALES & MARKETING 2 (OVERSEA) SEC ","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"22","ParentId":"21","Name":"QUALITY ASSURANCE DEPT.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"23","ParentId":"22","Name":"QC WHEELS GROUP","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"26","ParentId":"22","Name":"QC FOUNDRY GROUP","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"24","ParentId":"23","Name":"QUALITY CONTROL 2W SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"25","ParentId":"23","Name":"QUALITY CONTROL 4W SEC","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"27","ParentId":"26","Name":"QUALITY ENGINEERING SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"28","ParentId":"26","Name":"QUALITY CONTROL FD SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"30","ParentId":"29","Name":"FINANCE DEPT.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"31","ParentId":"30","Name":"FINANCE SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"33","ParentId":"32","Name":"ACCOUNTING DEPT.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"34","ParentId":"33","Name":"GEN.  ACCOUNTING SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"35","ParentId":"33","Name":"COST  ACCOUNTING SEC.COST  ACCOUNTING SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"37","ParentId":"36","Name":"PURCHASING & W/H DEPT.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"38","ParentId":"37","Name":"PURCHASING SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"39","ParentId":"37","Name":"WAREHOUSE SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"41","ParentId":"37","Name":"PART CONTROL SEC. ","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"40","ParentId":"39","Name":"Warehouse  Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"43","ParentId":"42","Name":"HUMAN RESOURCES MANAGEMENT DEPT.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"47","ParentId":"42","Name":"GENERAL AFFAIRS DEPT.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"44","ParentId":"43","Name":"EMPLOYEE RELATION. SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"45","ParentId":"43","Name":"COMPENSATION SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"46","ParentId":"43","Name":"HUMAN RESOURCES DEVELOPMENT SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"48","ParentId":"47","Name":"GENERAL AFFAIRS SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"50","ParentId":"47","Name":"ENVIRONMENT SEC. ","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"49","ParentId":"48","Name":"General Affairs Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"51","ParentId":"50","Name":"4W Production Control","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"52","ParentId":"50","Name":"4W Delivery","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"54","ParentId":"53","Name":"PRODUCTION CONTROL DEPT.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"59","ParentId":"54","Name":"DELIVERY SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"58","ParentId":"54","Name":"LOT CONTROL SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"55","ParentId":"54","Name":"4W PRODUCTION CONTROL SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"56","ParentId":"54","Name":"INVENTORY CONTROL SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"57","ParentId":"54","Name":"WAREHOUSE SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"61","ParentId":"60","Name":"4 W GROUP DEPT.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"127","ParentId":"60","Name":"2W GROUP DEPT.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"96","ParentId":"61","Name":"4W GROUP - 2","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"62","ParentId":"61","Name":"PRODUCTION GROUP","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"82","ParentId":"61","Name":"4W GROUP -1","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"106","ParentId":"61","Name":"PAINT & PACK GROUP DEPT.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"101","ParentId":"61","Name":"4W SUPPORT GROUP","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"63","ParentId":"62","Name":"MAC 1 SEC.(MAP 1)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"71","ParentId":"62","Name":"MAC 2 SEC.(MAP 2)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"72","ParentId":"62","Name":"MAC 3 SEC.(MAP 3)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"77","ParentId":"62","Name":"MAC 4 SEC.(MAP 4)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"64","ParentId":"63","Name":"MAC 1  Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"65","ParentId":"64","Name":"MAC 1 (A) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"66","ParentId":"64","Name":"MAC 1 (B) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"67","ParentId":"64","Name":"MAC 1 (C) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"68","ParentId":"64","Name":"MAC 2 (A) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"69","ParentId":"64","Name":"MAC 2 (B) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"70","ParentId":"64","Name":"MAC 2 (C) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"73","ParentId":"72","Name":"MAC 3 Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"74","ParentId":"73","Name":"MAC 3 (A) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"75","ParentId":"73","Name":"MAC 3 (B) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"76","ParentId":"73","Name":"MAC 3 (C) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"78","ParentId":"77","Name":"MAC 4 Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"79","ParentId":"78","Name":"MAC 4 (A) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"80","ParentId":"78","Name":"MAC 4 (B) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"81","ParentId":"78","Name":"MAC 4 (C) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"83","ParentId":"82","Name":"MAC 5 SEC.(MAP 5)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"88","ParentId":"82","Name":"MAC 6 SEC.(MAP 6)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"92","ParentId":"82","Name":"MAC 7 SEC.(MAP 7)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"84","ParentId":"83","Name":"MAC 5 Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"85","ParentId":"84","Name":"MAC 5 (A) ","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"86","ParentId":"84","Name":"MAC 5 (B) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"87","ParentId":"84","Name":"MAC 5 (C) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"89","ParentId":"88","Name":"MAC 6  (A) ","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"90","ParentId":"88","Name":"MAC 6  (B) ","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"91","ParentId":"88","Name":"MAC 6  (C) ","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"93","ParentId":"92","Name":"MAC 7 (A)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"94","ParentId":"92","Name":"MAC 7 (B)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"95","ParentId":"92","Name":"MAC 7 (C)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"97","ParentId":"96","Name":"MAC-C (MAC 8-10)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"98","ParentId":"96","Name":"MAC-D (MAC 11-13)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"99","ParentId":"96","Name":"MAC-E (MAC 14-15)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"100","ParentId":"99","Name":"MAC E (14-15)Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"102","ParentId":"101","Name":"ADMINISTRATION","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"104","ParentId":"101","Name":"MOLD PREPARATION","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"105","ParentId":"101","Name":"PRODUCTION DEVELOPMENT","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"103","ParentId":"102","Name":"MAC  Administration Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"107","ParentId":"106","Name":"PAINT PLANING SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"123","ParentId":"106","Name":"VR SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"108","ParentId":"106","Name":"PAINTING 2 SEC. ","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"113","ParentId":"106","Name":"PACKING 2 SEC","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"118","ParentId":"106","Name":"PAINTING & PACKING 3 SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"190","ParentId":"106","Name":"PACKING 3 (A) ","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"191","ParentId":"106","Name":"PACKING 3 (B) ","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"109","ParentId":"108","Name":"Painting 2","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"110","ParentId":"109","Name":"Painting 2 (A)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"111","ParentId":"109","Name":"Painting 2 (B)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"112","ParentId":"109","Name":"Painting 2 (C)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"114","ParentId":"113","Name":"Packing 2 ","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"115","ParentId":"114","Name":"Packing 2 (A)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"116","ParentId":"114","Name":"Packing 2 (B)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"117","ParentId":"114","Name":"Packing 2 (C)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"119","ParentId":"118","Name":"Painting 3 Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"122","ParentId":"118","Name":"PACKING 3","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"120","ParentId":"119","Name":"Painting 3 (A) ","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"121","ParentId":"119","Name":"Painting 3 (C) ","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"124","ParentId":"123","Name":"Value Recovery2 (A) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"125","ParentId":"123","Name":"Value Recovery 2(B) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"126","ParentId":"123","Name":"Value Recovery2 (C) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"151","ParentId":"127","Name":"2W PRODUCTION CONTROL SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"128","ParentId":"127","Name":"2 W CNC SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"138","ParentId":"127","Name":"2 W ASSY SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"133","ParentId":"127","Name":"MCC SEC. ","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"142","ParentId":"127","Name":"PAINTING 1 SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"148","ParentId":"127","Name":"2W PRODUCTION PLANING SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"149","ParentId":"127","Name":"2W  MCC F1","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"150","ParentId":"127","Name":"2W HIGH PRESSURE DIE CASTING","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"129","ParentId":"128","Name":"2W CNC. Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"130","ParentId":"129","Name":"2W CNC. (A) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"131","ParentId":"129","Name":"2W CNC. (B) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"132","ParentId":"129","Name":"2W CNC. (C) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"134","ParentId":"133","Name":"MCC 1 Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"135","ParentId":"134","Name":"MCC 1 (A)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"136","ParentId":"134","Name":"MCC 1 (B)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"137","ParentId":"134","Name":"MCC 1 (C)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"139","ParentId":"138","Name":"2W Assy  (A)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"140","ParentId":"138","Name":"2W Assy (B)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"141","ParentId":"138","Name":"2W Assy (C)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"143","ParentId":"142","Name":"PAINTING 1 (office)Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"147","ParentId":"142","Name":"PAINTING 1 Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"144","ParentId":"143","Name":"PAINTING 1 (A)Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"145","ParentId":"143","Name":"PAINTING 1 (B)Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"146","ParentId":"143","Name":"PAINTING 1 (C)Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"152","ParentId":"151","Name":"2W Production Control","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"154","ParentId":"151","Name":"2W Delivery","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"153","ParentId":"152","Name":"2W Production Control (A)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"159","ParentId":"155","Name":"MANAGEMENT SYSTEM CENTER SECTION (MSC)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"156","ParentId":"155","Name":"PRODUCTION ENGINEERING DEPT.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"157","ParentId":"156","Name":"MACHINING","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"158","ParentId":"156","Name":"SUPPORT EJT","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"161","ParentId":"160","Name":"FOUNDRY GROUP DEPT.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"175","ParentId":"161","Name":"FOUNDRY CONTROL CENTER SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"179","ParentId":"161","Name":"FD PRODUCTION CONTROL SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"180","ParentId":"161","Name":"FD MOLD CENTER SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"162","ParentId":"161","Name":"FOUNDRY 1 SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"172","ParentId":"161","Name":"FOUNDRY 3 SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"167","ParentId":"161","Name":"FOUNDRY 2 SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"163","ParentId":"162","Name":"Foundry 1 - LPD (Office) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"166","ParentId":"162","Name":"Foundry 1 - GDC","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"164","ParentId":"163","Name":"Foundry 1 - LPD (A) Sect","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"165","ParentId":"163","Name":"Foundry 1 - LPD (B) Sect","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"168","ParentId":"167","Name":"Foundry 2 (Office) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"169","ParentId":"168","Name":"Foundry 2 (A) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"170","ParentId":"168","Name":"Foundry 2 (B) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"171","ParentId":"168","Name":"Foundry 2 (C) Sect.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"173","ParentId":"172","Name":"Foundry 3 (A)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"174","ParentId":"172","Name":"Foundry 3 (B)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"176","ParentId":"175","Name":"Foundry Control Center (Office)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"177","ParentId":"176","Name":"Foundry Control Center (A) ","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"178","ParentId":"176","Name":"Foundry Control Center (B)","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"182","ParentId":"181","Name":"REPAIR  AND  MAINTENANCE SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"186","ParentId":"181","Name":"FACILITIES AND PROJECT","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"183","ParentId":"182","Name":"PAINT MAINTENANCE SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"184","ParentId":"182","Name":"PROCESS REPAIR SEC","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"185","ParentId":"182","Name":"M/N AUDIT & DEVELOPMENT SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"189","ParentId":"186","Name":"TECHNICAL SKILL & DEVELOPMENT","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"187","ParentId":"186","Name":"FACILITIES SEC.","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"},{"Id":"188","ParentId":"186","Name":"P.I.E SEC","RenderAsAssistant":"False","Url":"","IsManager":"boxGray","PositionName":"","Email":"","Phone":"","DisplayDep":"display:","DisplayEmp":"display:none","Icon":"Division-Icon.png","CountChild":"190","Type":"dept","ParentType":"dept"}];		
		var dumpJson2 = [
			{
				"Id":"9999999",
				"ParentId":"0",
				"Name":"ID 0",
				"RenderAsAssistant":"False",
				"Url":"",
				"IsManager":"boxGray",
				"PositionName":"",
				"Email":"",
				"Phone":"",
				"DisplayDep":"display:",
				"DisplayEmp":"display:none",
				"Icon":"Calendar-icon.png",
				"CountChild":"",
				"Type":"dept",
				"ParentType":"org"
			},{
				"Id":"1",
				"ParentId":"9999999",
				"Name":"ID 1",
				"RenderAsAssistant":"False",
				"Url":"",
				"IsManager":"boxGray",
				"PositionName":"",
				"Email":"",
				"Phone":"",
				"DisplayDep":"display:",
				"DisplayEmp":"display:none",
				"Icon":"Calendar-icon.png",
				"CountChild":"",
				"Type":"dept",
				"ParentType":"dept"
			}
		];
		var dumpJson3 = [{"Columns":[{"ColumnName":"SequenceNo","DataField":"SequenceNo","DataType":"Int64","ColumnWidth":"6%"},{"ColumnName":"ShotFlowHeaderOID","DataField":"ShotFlowHeaderOID","DataType":"Int32","ColumnWidth":"6%"},{"ColumnName":"FormOID","DataField":"FormOID","DataType":"Int32","ColumnWidth":"6%"},{"ColumnName":"StepNo","DataField":"StepNo","DataType":"Int64","ColumnWidth":"6%"},{"ColumnName":"StepName","DataField":"StepName","DataType":"String","ColumnWidth":"6%"},{"ColumnName":"StepApproverOID","DataField":"StepApproverOID","DataType":"Int32","ColumnWidth":"6%"},{"ColumnName":"StepApproverName","DataField":"StepApproverName","DataType":"String","ColumnWidth":"6%"},{"ColumnName":"ActionName","DataField":"ActionName","DataType":"String","ColumnWidth":"6%"},{"ColumnName":"ActionByOID","DataField":"ActionByOID","DataType":"Int32","ColumnWidth":"6%"},{"ColumnName":"ActionByName","DataField":"ActionByName","DataType":"String","ColumnWidth":"6%"},{"ColumnName":"Comment","DataField":"Comment","DataType":"String","ColumnWidth":"6%"},{"ColumnName":"Description","DataField":"Description","DataType":"String","ColumnWidth":"6%"},{"ColumnName":"CreateDate","DataField":"CreateDate","DataType":"DateTime","ColumnWidth":"6%"},{"ColumnName":"CurrentStep","DataField":"CurrentStep","DataType":"Int32","ColumnWidth":"6%"},{"ColumnName":"SeqNo","DataField":"SeqNo","DataType":"String","ColumnWidth":"6%"},{"ColumnName":"LastWaiting","DataField":"LastWaiting","DataType":"Int32","ColumnWidth":"6%"}],"Rows":[{"SequenceNo":"0","ShotFlowHeaderOID":"6834","FormOID":"10776","StepNo":"0","StepName":"Create","StepApproverOID":"1","StepApproverName":"administrator","ActionName":"Create","ActionByOID":"1","ActionByName":"administrator","Comment":"","Description":"Create by administrator","CreateDate":"30/01/2017 16:44","CurrentStep":"0","SeqNo":"1","LastWaiting":"0"},{"SequenceNo":"1","ShotFlowHeaderOID":"6834","FormOID":"10776","StepNo":"1","StepName":"User Approve (VP)","StepApproverOID":"2","StepApproverName":"Paisarn   Trichavaroj","ActionName":"Waiting","ActionByOID":"2","ActionByName":"Paisarn   Trichavaroj","Comment":"","Description":"","CreateDate":"","CurrentStep":"0","SeqNo":"2","LastWaiting":"1"},{"SequenceNo":"2","ShotFlowHeaderOID":"6834","FormOID":"10776","StepNo":"2","StepName":"App Support Approve (M)","StepApproverOID":"6","StepApproverName":"Utaiwan Wutthikunaporn","ActionName":"Waiting","ActionByOID":"6","ActionByName":"Utaiwan Wutthikunaporn","Comment":"","Description":"","CreateDate":"","CurrentStep":"0","SeqNo":"2","LastWaiting":"1"},{"SequenceNo":"3","ShotFlowHeaderOID":"6834","FormOID":"10776","StepNo":"3","StepName":"VP Division: Marketing & IMC","StepApproverOID":"0","StepApproverName":"Pongthep Thanakijsuntorn","ActionName":"Waiting","ActionByOID":"5","ActionByName":"Pongthep Thanakijsuntorn","Comment":"","Description":"","CreateDate":"","CurrentStep":"0","SeqNo":"2","LastWaiting":"1"},{"SequenceNo":"4","ShotFlowHeaderOID":"6834","FormOID":"10776","StepNo":"4","StepName":"VP Department: Domestic Business","StepApproverOID":"0","StepApproverName":"Apirath   Wisitthiwong","ActionName":"Waiting","ActionByOID":"20","ActionByName":"Apirath   Wisitthiwong","Comment":"","Description":"","CreateDate":"","CurrentStep":"0","SeqNo":"2","LastWaiting":"1"},{"SequenceNo":"5","ShotFlowHeaderOID":"6834","FormOID":"10776","StepNo":"5","StepName":"VP Section: ISP Account","StepApproverOID":"0","StepApproverName":"Apirath   Wisitthiwong","ActionName":"Waiting","ActionByOID":"20","ActionByName":"Apirath   Wisitthiwong","Comment":"","Description":"","CreateDate":"","CurrentStep":"0","SeqNo":"2","LastWaiting":"1"},{"SequenceNo":"6","ShotFlowHeaderOID":"6834","FormOID":"10776","StepNo":"6","StepName":"IT VP Approve","StepApproverOID":"5","StepApproverName":"Paisarn Trichavaroj","ActionName":"Waiting","ActionByOID":"5","ActionByName":"Paisarn Trichavaroj","Comment":"","Description":"","CreateDate":"","CurrentStep":"0","SeqNo":"2","LastWaiting":"1"}]}];
		var dumpJson4 = [{"d":{"Columns":[{"ColumnName":"SequenceNo","DataField":"SequenceNo","DataType":"Int64","ColumnWidth":"5%"},{"ColumnName":"ShotFlowHeaderOID","DataField":"ShotFlowHeaderOID","DataType":"Int32","ColumnWidth":"5%"},{"ColumnName":"FormOID","DataField":"FormOID","DataType":"Int32","ColumnWidth":"5%"},{"ColumnName":"StepNo","DataField":"StepNo","DataType":"Int64","ColumnWidth":"5%"},{"ColumnName":"StepName","DataField":"StepName","DataType":"String","ColumnWidth":"5%"},{"ColumnName":"StepApproverOID","DataField":"StepApproverOID","DataType":"Int32","ColumnWidth":"5%"},{"ColumnName":"StepApproverName","DataField":"StepApproverName","DataType":"String","ColumnWidth":"5%"},{"ColumnName":"ActionName","DataField":"ActionName","DataType":"String","ColumnWidth":"5%"},{"ColumnName":"ActionByOID","DataField":"ActionByOID","DataType":"Int32","ColumnWidth":"5%"},{"ColumnName":"ActionByName","DataField":"ActionByName","DataType":"String","ColumnWidth":"5%"},{"ColumnName":"Comment","DataField":"Comment","DataType":"String","ColumnWidth":"5%"},{"ColumnName":"Description","DataField":"Description","DataType":"String","ColumnWidth":"5%"},{"ColumnName":"CreateDate","DataField":"CreateDate","DataType":"DateTime","ColumnWidth":"5%"},{"ColumnName":"CurrentStep","DataField":"CurrentStep","DataType":"Int32","ColumnWidth":"5%"},{"ColumnName":"SeqNo","DataField":"SeqNo","DataType":"String","ColumnWidth":"5%"},{"ColumnName":"LastWaiting","DataField":"LastWaiting","DataType":"Int32","ColumnWidth":"5%"},{"ColumnName":"IsDisabled","DataField":"IsDisabled","DataType":"Int32","ColumnWidth":"5%"},{"ColumnName":"TransConditionHeaderOID","DataField":"TransConditionHeaderOID","DataType":"Int32","ColumnWidth":"5%"},{"ColumnName":"StepDetailTransOID","DataField":"StepDetailTransOID","DataType":"Int32","ColumnWidth":"5%"},{"ColumnName":"TransConditionType","DataField":"TransConditionType","DataType":"Int32","ColumnWidth":"5%"}],"Rows":[{"SequenceNo":"0","ShotFlowHeaderOID":"6844","FormOID":"10786","StepNo":"0","StepName":"Create","StepApproverOID":"1","StepApproverName":"administrator","ActionName":"Create","ActionByOID":"1","ActionByName":"administrator","Comment":"","Description":"Create by administrator","CreateDate":"31/01/2017 07:52","CurrentStep":"0","SeqNo":"1","LastWaiting":"0","IsDisabled":"0","TransConditionHeaderOID":"0","StepDetailTransOID":"0","TransConditionType":""},{"SequenceNo":"1","ShotFlowHeaderOID":"6844","FormOID":"10786","StepNo":"1","StepName":"User Approve (VP)","StepApproverOID":"2","StepApproverName":"Paisarn   Trichavaroj","ActionName":"Waiting","ActionByOID":"2","ActionByName":"Paisarn   Trichavaroj","Comment":"","Description":"","CreateDate":"","CurrentStep":"1","SeqNo":"2","LastWaiting":"1","IsDisabled":"0","TransConditionHeaderOID":"0","StepDetailTransOID":"9423","TransConditionType":""},{"SequenceNo":"2","ShotFlowHeaderOID":"6844","FormOID":"10786","StepNo":"2","StepName":"App Support Approve (M)","StepApproverOID":"6","StepApproverName":"Utaiwan Wutthikunaporn","ActionName":"Waiting","ActionByOID":"6","ActionByName":"Utaiwan Wutthikunaporn","Comment":"","Description":"","CreateDate":"","CurrentStep":"0","SeqNo":"2","LastWaiting":"1","IsDisabled":"0","TransConditionHeaderOID":"0","StepDetailTransOID":"9424","TransConditionType":""},{"SequenceNo":"3","ShotFlowHeaderOID":"6844","FormOID":"10786","StepNo":"3","StepName":"[NewRouteByAppSupportApprove]","StepApproverOID":"0","StepApproverName":"","ActionName":"Waiting","ActionByOID":"","ActionByName":"","Comment":"","Description":"","CreateDate":"","CurrentStep":"0","SeqNo":"2","LastWaiting":"1","IsDisabled":"1","TransConditionHeaderOID":"0","StepDetailTransOID":"9425","TransConditionType":"2"},{"SequenceNo":"4","ShotFlowHeaderOID":"6844","FormOID":"10786","StepNo":"4","StepName":"OSD Approve (VP)","StepApproverOID":"4","StepApproverName":"Chakkrit Sangsawang","ActionName":"Waiting","ActionByOID":"4","ActionByName":"Chakkrit Sangsawang","Comment":"","Description":"","CreateDate":"","CurrentStep":"0","SeqNo":"2","LastWaiting":"1","IsDisabled":"1","TransConditionHeaderOID":"9425","StepDetailTransOID":"9426","TransConditionType":""},{"SequenceNo":"5","ShotFlowHeaderOID":"6844","FormOID":"10786","StepNo":"5","StepName":"VP Division: Marketing u0026 IMC","StepApproverOID":"5","StepApproverName":"Paisarn Trichavaroj","ActionName":"Waiting","ActionByOID":"5","ActionByName":"Paisarn Trichavaroj","Comment":"","Description":"","CreateDate":"","CurrentStep":"0","SeqNo":"2","LastWaiting":"1","IsDisabled":"1","TransConditionHeaderOID":"9425","StepDetailTransOID":"9427","TransConditionType":""},{"SequenceNo":"6","ShotFlowHeaderOID":"6844","FormOID":"10786","StepNo":"6","StepName":"VP Department: Domestic Business","StepApproverOID":"20","StepApproverName":"Apirath   Wisitthiwong","ActionName":"Waiting","ActionByOID":"20","ActionByName":"Apirath   Wisitthiwong","Comment":"","Description":"","CreateDate":"","CurrentStep":"0","SeqNo":"2","LastWaiting":"1","IsDisabled":"1","TransConditionHeaderOID":"9425","StepDetailTransOID":"9428","TransConditionType":""},{"SequenceNo":"7","ShotFlowHeaderOID":"6844","FormOID":"10786","StepNo":"7","StepName":"VP Section: ISP Account","StepApproverOID":"20","StepApproverName":"Apirath   Wisitthiwong","ActionName":"Waiting","ActionByOID":"20","ActionByName":"Apirath   Wisitthiwong","Comment":"","Description":"","CreateDate":"","CurrentStep":"0","SeqNo":"2","LastWaiting":"1","IsDisabled":"1","TransConditionHeaderOID":"9425","StepDetailTransOID":"9429","TransConditionType":""},{"SequenceNo":"8","ShotFlowHeaderOID":"6844","FormOID":"10786","StepNo":"8","StepName":"VP Section: International Service","StepApproverOID":"123","StepApproverName":"Somchai Treerattananukool","ActionName":"Waiting","ActionByOID":"123","ActionByName":"Somchai Treerattananukool","Comment":"","Description":"","CreateDate":"","CurrentStep":"0","SeqNo":"2","LastWaiting":"1","IsDisabled":"1","TransConditionHeaderOID":"9425","StepDetailTransOID":"9430","TransConditionType":""},{"SequenceNo":"9","ShotFlowHeaderOID":"6844","FormOID":"10786","StepNo":"9","StepName":"IT VP Approve","StepApproverOID":"5","StepApproverName":"Paisarn Trichavaroj","ActionName":"Waiting","ActionByOID":"5","ActionByName":"Paisarn Trichavaroj","Comment":"","Description":"","CreateDate":"","CurrentStep":"0","SeqNo":"2","LastWaiting":"1","IsDisabled":"1","TransConditionHeaderOID":"9425","StepDetailTransOID":"9431","TransConditionType":""},{"SequenceNo":"10","ShotFlowHeaderOID":"6844","FormOID":"10786","StepNo":"10","StepName":"[ExistRouteByAppSupportApprove]","StepApproverOID":"0","StepApproverName":"","ActionName":"Waiting","ActionByOID":"","ActionByName":"","Comment":"","Description":"","CreateDate":"","CurrentStep":"0","SeqNo":"2","LastWaiting":"1","IsDisabled":"1","TransConditionHeaderOID":"0","StepDetailTransOID":"9432","TransConditionType":"2"},{"SequenceNo":"11","ShotFlowHeaderOID":"6844","FormOID":"10786","StepNo":"11","StepName":"IT VP Approve","StepApproverOID":"5","StepApproverName":"Paisarn Trichavaroj","ActionName":"Waiting","ActionByOID":"5","ActionByName":"Paisarn Trichavaroj","Comment":"","Description":"","CreateDate":"","CurrentStep":"0","SeqNo":"2","LastWaiting":"1","IsDisabled":"1","TransConditionHeaderOID":"9432","StepDetailTransOID":"9433","TransConditionType":""}]}}];
		//console.log(dumpJson4[0]['d']['Rows']);
		
		for(i=1; i<10; i++)
		dumpJson2.push({
			"Id":i,
			"ParentId":i-1,
			"Name":"ID " + i,
			"RenderAsAssistant":"False",
			"Url":"",
			"IsManager":"boxGray",
			"PositionName":"",
			"Email":"",
			"Phone":"",
			"DisplayDep":"display:",
			"DisplayEmp":"display:none",
			"Icon":"Calendar-icon.png",
			"CountChild":"",
			"Type":"dept",
			"ParentType":"dept"			
		});
		for(i=10; i<15; i++){
			console.log(i);
			dumpJson2.push({
				"Id":i,
				"ParentId":i-(i==10?7:1),
				"Name":"ID " + i,
				"RenderAsAssistant":"False",
				"Url":"",
				"IsManager":"boxGray",
				"PositionName":"",
				"Email":"",
				"Phone":"",
				"DisplayDep":"display:",
				"DisplayEmp":"display:none",
				"Icon":"Calendar-icon.png",
				"CountChild":"",
				"Type":"dept",
				"ParentType":"dept"			
			});
		}
		
		
		
		var dbJsonRows = dumpJson4[0]['d']['Rows'];
		for(i in dbJsonRows){
			if(tree.indexOf(dbJsonRows[i].ParentId) == -1)
				tree[tree.length] = dbJsonRows[i].Id;
			addItem(dbJsonRows[i]);
		}
		
		
		
		var activeId = 10;
		
		$('.card.card-group.id-' + (activeId+1) + '.type-dept .card > .before').css('border-left-color', '#ccc');
		$('.card.card-group.id-' + (activeId+1) + '.type-dept .card > .after ').css('border-left-color', '#ccc');
		
		$('.card.card-group.id-' + (activeId+1) + '.type-dept .detail > .before ').css('border-bottom-color', '#ccc');
		$('.card.card-group.id-' + (activeId) + '.type-dept .detail > .after ').css('border-bottom-color', '#ccc');
		
		$('.card.card-group.id-' + (activeId+1) + '.type-dept .detail button').css('cssText', 'border-color:#ccc; box-shadow:0 0 0 1px #ccc;');
		$('.card.card-group.id-' + (activeId+1) + '.type-dept .detail button > h3').css('cssText', 'background-color:#ccc;');
		$('.card.card-group.id-' + (activeId+1) + '.type-dept .detail button > span.after').css('border-top-color', '#ccc');
		
		
		activeId = 4;
		$('.card.card-group.id-' + (activeId) + '.type-dept > .before').css('border-left-color', '#ccc');
		
		$('.card.card-group.id-' + (activeId) + '.type-dept .card > .before').css('border-left-color', '#ccc');
		$('.card.card-group.id-' + (activeId) + '.type-dept .card > .after ').css('border-left-color', '#ccc');
		
		$('.card.card-group.id-' + (activeId) + '.type-dept .detail > .before ').css('border-bottom-color', '#ccc');
		$('.card.card-group.id-' + (activeId) +'.type-dept .detail > .after ').css('border-bottom-color', '#ccc');
		
		$('.card.card-group.id-' + (activeId) + '.type-dept .detail button').css('cssText', 'border-color:#ccc; box-shadow:0 0 0 1px #ccc;');
		$('.card.card-group.id-' + (activeId) + '.type-dept .detail button > h3').css('cssText', 'background-color:#ccc;');
		$('.card.card-group.id-' + (activeId) + '.type-dept .detail button > span.after').css('border-top-color', '#ccc');
		
		
		
		$('.parent:not([ready])').click(pinchart);
		$('.parent').attr('ready', true);

		brotherline();
		resizecard();
		fitscreen();
		
		$('#opens').click();
		
		
		/*
		var requestApi = window.location.href + '?method=' + _orgchartMethod;
		$.ajax({
			type: "GET",
			url: requestApi,
			success: function (res) {      
				//console.log(res.Rows);      
				
				var dbJsonRows = res.Rows;
				for(i in dbJsonRows){
					if(tree.indexOf(dbJsonRows[i].ParentId) == -1)
						tree[tree.length] = dbJsonRows[i].Id;
					addItem(dbJsonRows[i]);
				}
				
				$('.parent:not([ready])').click(pinchart);
				$('.parent').attr('ready', true);

				brotherline();
				resizecard();
				fitscreen();
				
				$('#opens').click();
			}
		});
		*/
	};


	function getOrganize(qtype, qid){
		var _orgchartMethod = 'member';
		if(orgchartMethod)
			_orgchartMethod = orgchartMethod;
		var requestApi = window.location.href + '?type=' + qtype + '&id=' + qid + '&method=' + _orgchartMethod;
		$.ajax({
			type: "GET",
			url: requestApi,
			success: function (res) {
				//console.warn('Cross code file.');
				//console.log(requestApi);
				//console.log(res.Rows);

				var Rows = res.Rows;
				if(Rows.length == 0)
					$('.id-' + qid + '.type-' + qtype).addClass('empty');
				else
					for(i in Rows){
						if(tree.indexOf(Rows[i].ParentId) == -1)
							tree[tree.length] = Rows[i].Id;
						addItem(Rows[i]);
					}

				brotherline();
				resizecard();

				$('.id-' + qid + ' > .detail[data-type="' + qtype + '"] .loader').remove();

				$('.parent:not([ready])').click(pinchart);
				$('.parent').attr('ready', true);
				
				fitscreen();
			}
		});
	};



	var paper = 4;
	var papermax = 4;
	var papersize = [];
	var paperflip = 'x';
	papersize[1] = { 'x' : 2384, 'y' : 3370 };
	papersize[2] = { 'x' : 1684, 'y' : 2384 };
	papersize[3] = { 'x' : 1191, 'y' : 1684 };
	papersize[4] = { 'x' : 842, 'y' : 1191 };
	$('#papersize').click(function(){
		--paper;
		if(paper < 1)
			paper = papermax;
		
		$('#papersize span').html('A'+paper);
		fitscreen();
	});
	$('#paperflip').click(function(){
		$(this).toggleClass('flip');
		switch(paperflip){
			case 'x':
				paperflip = 'y';
				break;
			case 'y':
				paperflip = 'x';
				break;
		}
		fitscreen();	
	});

	function fitscreen(){	
		/* Disable with same paper size
		$('.orgchart').css('transform', 'scale(1)');
		var windowWidth = papersize[paper][paperflip];

		$('.orgchart').each(function(){
			if($(this).width() > windowWidth)
				$(this).css('transform', 'scale(' + ((windowWidth/ $(this).width())) + ')');
		});
		*/
	}
	function printscreen(){	
		if(paperflip=='x'){
			var windowWidth = papersize[paper]['y'];
			var windowHeight = papersize[paper]['x'];
		}else{
			var windowWidth = papersize[paper]['x'];
			var windowHeight = papersize[paper]['y'];
		}
		windowWidth -= 100;
		windowHeight -= 100;
		lockscene(windowWidth, windowHeight);
			
		var thisSize = 0;
			
		$('.orgchart').css('transform', 'scale(1)');
		$('.orgchart').each(function(){
			if($(this).width() > $(this).height()) thisSize = $(this).width();
			else thisSize = $(this).height();        
			
			if(thisSize > windowWidth){
				$(this).css('transform', 'scale(' + (windowWidth/ thisSize) + ')');
				
				if($(this).width() > $(this).height()) thisSize = $(this).width();
				else thisSize = $(this).height();        
			}
			if(thisSize > windowHeight)
				$(this).css('transform', 'scale(' + (windowHeight/ thisSize) + ')');
				
			//console.log(paper);
			//console.log($(this).height(), windowHeight);
		}); 
	}
	function resetscreen(){	
		$('.orgchart').css('transform', 'scale(1)');
	}
	function lockscene(w,h){
		$('body').css({'width':w, 'height':h, 'overflow':'hidden'});
	}
	function releasescene(){
		$('body').css({'width':'auto', 'height':'auto', 'overflow':'visible'});
	}



	$('.layout').click(function(){
		$('.orgchart').removeClass('orgchart-step');
		$('.orgchart').removeClass('orgchart-horizontal');
		$('.orgchart').removeClass('orgchart-vertical');
		$('.orgchart').addClass('orgchart-' + layouts[$(this).data('id')]);
		fitscreen();	
		
		orgchart_layout = $(this).data('id');
	});



	var viewzoom = 1;
	$('#zoomin').click(function(){
		viewzoom += 0.1;
		$('.orgchart').css('transform', 'scale('+viewzoom+')');
	});
	$('#zoomout').click(function(){
		if(viewzoom > 0.2)
		viewzoom -= 0.1;
		$('.orgchart').css('transform', 'scale('+viewzoom+')');
	});


	//Ready
	if(params.overload){
		$('#overload').addClass('hidden');
		$('#opens').click();
		
		if(params.layout)
			$('[data-id="' + params.layout + '"]').click();
		
		if(params.dept){
			$('.type-dept.id-' + params.dept + ' .parent').eq(0).click();
			$('#opens').click();
		}
			
	}
	resizecard();
	fitscreen();


});